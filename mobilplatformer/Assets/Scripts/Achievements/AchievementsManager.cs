using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Molioo.Achievements
{
    public class AchievementsManager : Singleton<AchievementsManager>
    {
        private const string FILE_NAME = "Achievements.json";
        private static string FILE_PATH { get { return Application.persistentDataPath + "/" + FILE_NAME; } }

        [SerializeField]
        private List<AchievementPreset> _achievementsPresets = new List<AchievementPreset>();

        private AchievementsList _currentAchievements;

        public void Start()
        {
            LoadAchievements();
        }

        /// <summary>
        /// Saves current achievements to file as json
        /// </summary>
        public void SaveAchievements()
        {
            if (_currentAchievements == null)
            {
                _currentAchievements = new AchievementsList();
            }
            if (_currentAchievements.Achievements == null || _currentAchievements.Achievements.Count == 0)
            {
                _currentAchievements.Achievements = CreateNewAchievements();
            }

            StreamWriter file = new StreamWriter(FILE_PATH, false);
            file.Write(JsonUtility.ToJson(_currentAchievements));
            file.Close();
        }

        /// <summary>
        /// Tries to load achievements from file. If there is no file or something is wrong, achievements are created as new.
        /// </summary>
        public void LoadAchievements()
        {
            try
            {
                string jsonStats = File.ReadAllText(FILE_PATH);
                if (string.IsNullOrEmpty(jsonStats))
                {
                    Debug.Log("File was empty or nulled");
                    _currentAchievements = new AchievementsList();
                    _currentAchievements.Achievements = CreateNewAchievements();
                }
                else
                {
                    AchievementsList achievementsFromFile = JsonUtility.FromJson<AchievementsList>(jsonStats);
                    _currentAchievements = achievementsFromFile;
                    Debug.Log("Succesfully loaded achievements from file");
                }
            }
            catch (Exception e)
            {
                _currentAchievements = new AchievementsList();
                _currentAchievements.Achievements = CreateNewAchievements();

                Debug.Log("Something went wrong when reading file : " + e.Message);
                return;
            }
        }

        private List<AchievementData> CreateNewAchievements()
        {
            List<AchievementData> achievements = new List<AchievementData>();

            foreach (AchievementPreset achPreset in _achievementsPresets)
            {
                AchievementData newAch = new AchievementData(achPreset);
                achievements.Add(newAch);
            }
            return achievements;
        }


        public void UnlockAchievement(EAchievementType achievementType)
        {
            if (_currentAchievements == null)
            {
                Debug.LogError("No current achievements");
                return;
            }

            AchievementData achievement = _currentAchievements.GetAchievement(achievementType);
            if (achievement == null)
            {
                achievement = PrepareEmptyAchievement(achievementType);
                _currentAchievements.AddAchievement(achievement);
            }

            if (achievement == null)
                return;

            if (achievement.IsAchieved)
                return;

            UnlockOnGooglePlay(achievement);
            achievement.Unlock();
            SaveAchievements();
        }

        public AchievementData PrepareEmptyAchievement(EAchievementType achievementType)
        {
            foreach (AchievementPreset achPreset in _achievementsPresets)
            {
                if (achPreset.AchievementType == achievementType)
                    return new AchievementData(achPreset);
            }
            return null;
        }

        public void ProgressAchievement(EAchievementType achievementType, int progressValue)
        {
            if (_currentAchievements == null)
            {
                Debug.LogError("No current achievements");
                return;
            }

            AchievementData achievement = _currentAchievements.GetAchievement(achievementType);
            if (achievement == null)
                return;

            if (achievement.IsAchieved)
                return;

            achievement.Progress(progressValue);
            ProgressOnGooglePlay(achievement, progressValue);
            SaveAchievements();
        }

        private void UnlockOnGooglePlay(AchievementData achievementData)
        {
            if (SocialPlatformManager.Instance != null)
            {
                SocialPlatformManager.Instance.UnlockAchievement(achievementData);
            }
        }

        private void ProgressOnGooglePlay(AchievementData achievementData, int progress)
        {
            if (SocialPlatformManager.Instance != null)
            {
                SocialPlatformManager.Instance.ProgressAchievement(achievementData, progress);
            }
        }
    }
}