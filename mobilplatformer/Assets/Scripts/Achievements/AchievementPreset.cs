using UnityEngine;

namespace Molioo.Achievements
{
    [CreateAssetMenu(fileName = "AchievementPreset", menuName = "Achievements/Achievement Preset")]

    public class AchievementPreset : ScriptableObject
    {
        public EAchievementType AchievementType;
        public string ID;
        public int MaxValue = 1;
    }
}