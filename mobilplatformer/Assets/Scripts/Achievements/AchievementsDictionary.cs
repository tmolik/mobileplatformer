using System.Collections.Generic;
using UnityEngine;

namespace Molioo.Achievements
{
    /// <summary>
    /// Wrapper class for achievements list as there would be problems with jsonUtility and serialization of List of achievements if it weren't in class
    /// </summary>
    [System.Serializable]
    public class AchievementsDictionary
    {
        public Dictionary<EAchievementType, AchievementData> Achievements;

        public AchievementData GetAchievement(EAchievementType achType)
        {
            if (Achievements == null)
            {
                Debug.LogError("Achievements are null");
                return null;
            }

            if (Achievements.ContainsKey(achType))
                return Achievements[achType];

            return null;
        }
    }
}