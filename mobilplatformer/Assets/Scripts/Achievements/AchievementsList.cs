using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Molioo.Achievements
{
    /// <summary>
    /// Wrapper class for achievements list as there would be problems with jsonUtility and serialization of List of achievements if it weren't in class
    /// </summary>
    [System.Serializable]
    public class AchievementsList
    {
        public List<AchievementData> Achievements;

        public AchievementData GetAchievement(EAchievementType achType)
        {
            if (Achievements == null)
            {
                Debug.LogError("Achievements are null");
                return null;
            }

            for (int i = 0; i <= Achievements.Count - 1; i++)
            {
                if (Achievements[i].AchievementType == achType)
                    return Achievements[i];
            }

            return null;
        }

        public void AddAchievement(AchievementData achievement)
        {
            if (Achievements.Contains(achievement))
                return;
            Achievements.Add(achievement);
        }
    }
}