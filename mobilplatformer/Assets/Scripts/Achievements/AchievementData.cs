namespace Molioo.Achievements
{
    [System.Serializable]
    public class AchievementData
    {
        public EAchievementType AchievementType;
        public string AchievementId;
        public int CurrentValue = 0;
        public int MaxValue = 1;
        public bool IsAchieved = false;

        public AchievementData(AchievementPreset preset)
        {
            AchievementType = preset.AchievementType;
            AchievementId = preset.ID;
            CurrentValue = 0;
            MaxValue = preset.MaxValue;
            IsAchieved = false;
        }

        public void Unlock()
        {
            CurrentValue = MaxValue;
            IsAchieved = true;
        }

        public void Progress(int valueToAdd)
        {
            CurrentValue += valueToAdd;

            if (CurrentValue >= MaxValue)
                CurrentValue = MaxValue;

            IsAchieved = true;
        }
    }
}