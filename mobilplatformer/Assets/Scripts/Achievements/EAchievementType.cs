namespace Molioo.Achievements
{
    public enum EAchievementType
    {
        Beat10Levels,
        Beat25Levels,
        Beat50Levels,
        Beat75Levels,
        Survive60SecondsInEndless,
        Survive5MinInEndless,
        Beat100Levels
    }
}