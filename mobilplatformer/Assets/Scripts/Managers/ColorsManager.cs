using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ColorsManager : Singleton<ColorsManager>
{
    [SerializeField]
    private Material _playerMaterial = null;

    [SerializeField]
    private Material _obstacleMaterial = null;

    [SerializeField]
    private Material _backgroundMaterial = null;

    [SerializeField]
    private Material _levelObjectsMaterial = null;


    [SerializeField]
    private List<ColorsPreset> _colorPresetsList = new List<ColorsPreset>();

    [SerializeField]
    private ColorsPreset _currentColorsPreset = null;

    public void Start()
    {
        if (SettingsManager.CurrentSettings != null)
        {
            SetColorPresetWithId(SettingsManager.CurrentSettings.ColorPresetID);
        }
        else
        {
            SetColorPresetWithId(0);
        }
    }

    private void SetColorPresetWithId(int id)
    {
        foreach (ColorsPreset  preset  in _colorPresetsList)
        {
            if(preset.ColorPresetID == id)
            {
                _currentColorsPreset = preset;
                SetColors();
            }
        }
    }


    public void OnValidate()
    {
        if (_currentColorsPreset == null)
            return;

        SetColors();
    }

    public void Update()
    {
        HandleDebugInput();   
    }

    private void HandleDebugInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SetColors();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SetColorPresetWithId(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SetColorPresetWithId(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SetColorPresetWithId(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SetColorPresetWithId(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            SetColorPresetWithId(5);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            SetColorPresetWithId(6);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            SetColorPresetWithId(7);
        }
    }

    [ContextMenu("Set colors")]
    public void SetColors()
    {
        if (_playerMaterial != null)
        {
            _playerMaterial.SetColor("_BaseColor", _currentColorsPreset.PlayerColor);
        }

        if (_obstacleMaterial != null)
        {
            _obstacleMaterial.SetColor("_BaseColor", _currentColorsPreset.ObstaclesColor);
        }

        if (_backgroundMaterial != null)
        {
            _backgroundMaterial.SetColor("_BaseColor", _currentColorsPreset.BackgroundColor);
        }

        if (_levelObjectsMaterial != null)
        {
            _levelObjectsMaterial.SetColor("_BaseColor", _currentColorsPreset.BaseLevelObjectsColor);
        }
    }

    public void SetColorsPreset(ColorsPreset preset)
    {
        _currentColorsPreset = preset;
        SetColors();
    }

    public void SaveCurrentColorPreset()
    {
        SettingsManager.CurrentSettings.ColorPresetID = _currentColorsPreset.ColorPresetID;
        SettingsManager.SaveSettings();
    }

}
