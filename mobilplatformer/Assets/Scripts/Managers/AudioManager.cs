﻿using Molioo.ObjectPooling;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField]
    private AudioClip _uiClickAudioClip = null;

    [SerializeField]
    private AudioClip _playerDeathAudioClip = null;

    [SerializeField]
    private AudioClip _levelFinishedAudioClip = null;

    [SerializeField]
    private AudioClip _playerJumpAudioClip = null;


    public void PlayAudio2D(AudioClip clip)
    {
        if (clip == null)
            return;

        if (SettingsManager.CurrentSettings == null)
            return;

        if (SettingsManager.CurrentSettings.IsMuted)
            return;

        AudioSource audioSource = ObjectPooler.Get.GetPooledObject(EPoolables.AudioSource2D).GetComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.gameObject.SetActive(true);
        audioSource.spatialBlend = 0;
        audioSource.Play();
    }
    

    public void PlayUiClickSound()
    {
        PlayAudio2D(_uiClickAudioClip);
    }

    public void PlayPlayerDeathSound()
    {
        PlayAudio2D(_playerDeathAudioClip);
    }

    public void PlayLevelFinishedSound()
    {
        PlayAudio2D(_levelFinishedAudioClip);
    }

    public void PlayPlayerJumpSound()
    {
        PlayAudio2D(_playerJumpAudioClip);
    }
}
