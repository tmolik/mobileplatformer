﻿using UnityEngine;

public class GameplaySettingsManager : Singleton<GameplaySettingsManager>
{
    public GameplaySettings CurrentGameSettings;

    public delegate void OnSettingsChanged();

    public event OnSettingsChanged OnGameSettingsChanged;

    public void Start()
    {
        if (CurrentGameSettings == null)
            return;

        CurrentGameSettings.SetCurrentGameSettingsManager(this);
        SetupGravity();
        OnGameSettingsChanged += SetupGravity;
    }

    public void OnDestroy()
    {
        OnGameSettingsChanged -= SetupGravity;
    }

    public void SetupGravity()
    {
        Physics.gravity = new Vector3(0, CurrentGameSettings.GravityYPower, 0);
    }

    public void SettingsChanged()
    {
        OnGameSettingsChanged();
    }
}
