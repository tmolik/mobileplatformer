﻿using System.Collections.Generic;
using Molioo.UBB.Tutorial;

public class GameData
{
    public int CurrentLevelID = 0;

    public List<TutorialPhaseSaveHelper> TutorialSaves = new List<TutorialPhaseSaveHelper>();

    public void PopulateNewTutorialSaves()
    {
        TutorialSaves = new List<TutorialPhaseSaveHelper>();
        TutorialSaves.Add(new TutorialPhaseSaveHelper(ETutorialPhase.TapToJump, false));
        TutorialSaves.Add(new TutorialPhaseSaveHelper(ETutorialPhase.JumpOnTrampoline, false));
        TutorialSaves.Add(new TutorialPhaseSaveHelper(ETutorialPhase.GoThroughPortal, false));
    }

    public void ChangeTutorialSave(ETutorialPhase tutorial, bool wasSeen)
    {
        foreach(TutorialPhaseSaveHelper tutHelper in TutorialSaves)
        {
            if (tutHelper.TutorialPhase == tutorial)
                tutHelper.WasSeen = wasSeen;
        }
    }

    public bool WasTutorialSeen(ETutorialPhase tutorialPhase)
    {
        foreach (TutorialPhaseSaveHelper tutHelper in TutorialSaves)
        {
            if (tutHelper.TutorialPhase == tutorialPhase)
                return tutHelper.WasSeen;
        }

        return false;
    }
}
