using GooglePlayGames;
using GooglePlayGames.BasicApi;
using Molioo.Achievements;
using UnityEngine;

public class SocialPlatformManager : Singleton<SocialPlatformManager>
{
    private static string ENDLESS_LEADERBOARD_ID = "CgkI-sa66KcaEAIQBw";

    public void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        // enables saving game progress.
        //.EnableSavedGames()
        // requests the email address of the player be available.
        // Will bring up a prompt for consent.
        //.RequestEmail()
        // requests a server auth code be generated so it can be passed to an
        //  associated back end server application and exchanged for an OAuth token.
        //.RequestServerAuthCode(false)
        // requests an ID token be generated.  This OAuth token can be used to
        //  identify the player to other services such as Firebase.
        //.RequestIdToken()
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
        Authenticate();
    }

    private void Authenticate()
    {
        PlayGamesPlatform.Instance.Authenticate((bool success, string what) =>
       {
           Debug.Log("Authentication success " + success + " - " + what);
       });
    }


    public void UnlockAchievement(AchievementData achievement)
    {
        if (Social.Active != null)
        {
            Social.Active.ReportProgress(achievement.AchievementId, 100, AchievementCallback);
        }
    }

    private void AchievementCallback(bool success)
    {
        Debug.Log("Achievement unlock success? " + success);
    }

    public void ProgressAchievement(AchievementData achievement, int progress)
    {
        PlayGamesPlatform.Instance.IncrementAchievement(achievement.AchievementId, progress, ProgressAchievementCallback);
    }

    private void ProgressAchievementCallback(bool success)
    {
        Debug.Log("Achievement progress success? " + success);
    }

    public void UpdateLeaderboard(int score)
    {
        if (Social.Active != null)
        {
            Social.Active.ReportScore(score, ENDLESS_LEADERBOARD_ID, LeaderboardCallback);
        }
    }

    private void LeaderboardCallback(bool success)
    {
        Debug.Log("Leaderboard update success? " + success);
    }

    public void ShowAchievements()
    {
        if (Social.Active != null)
        {
            Social.Active.ShowAchievementsUI();
        }

    }

    public void ShowLeaderboard()
    {
        if (Social.Active != null)
        {
            Social.Active.ShowLeaderboardUI();
        }
    }
}
