﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Settings/GameSettings")]
public class GameplaySettings : ScriptableObject
{
    [Header("Camera")]
    public float CameraFoV = 60f;
    public float CameraZPosition = -10f;

    [Header("Player")]
    public float PlayerMovementSpeed = 10f;
    public float PlayerJumpPower = 6f;

    [Header("Other")]
    public float GravityYPower = -9.6f;

    private GameplaySettingsManager _gameSettingsManager;

    public void OnValidate()
    {
        if (_gameSettingsManager != null)
            _gameSettingsManager.SettingsChanged();
    }

    public void SetCurrentGameSettingsManager(GameplaySettingsManager settingsManager)
    {
        _gameSettingsManager = settingsManager;
    }

}
