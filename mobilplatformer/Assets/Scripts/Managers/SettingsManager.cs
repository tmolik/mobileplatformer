﻿using System;

public static class SettingsManager
{
    public static SettingsData CurrentSettings;

    public static event Action OnSettingsLoaded;

    public static void Initialize()
    {
        CurrentSettings = GameFilesManager.LoadSettings();
        GameFilesManager.SaveSettings(CurrentSettings);
        if (OnSettingsLoaded != null)
            OnSettingsLoaded.Invoke();
    }

    public static void SaveSettings()
    {
        GameFilesManager.SaveSettings(CurrentSettings);
    }
}
