using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

public class AdsManager : Singleton<AdsManager>, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener
{
    private const string ANDROID_GAME_ID = "4031199";

    private const string INTERSTITIAL_AD = "video";

    private const string REWARDED_AD = "rewardedVideo";

    private const string BANNER_AD = "menuBanner";

    private const int ADS_INTERVAL = 1 * 60;

    public static float TimeSinceLastAd = 0;

    private UnityAction _onWatchedRewardedVideo;

    [SerializeField]
    private bool _testMode = true;

    private bool _isRewardedAdLoaded = false;

    private bool _isBannerAdLoaded = false;

    private bool _isInterstitialAdLoaded = false;

    public void Start()
    {
        InitializeUnityAds();
    }

    public void InitializeUnityAds()
    {
        if (!Advertisement.isInitialized && Advertisement.isSupported)
        {
            Advertisement.Initialize(ANDROID_GAME_ID, _testMode, this);
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            Advertisement.Load(REWARDED_AD, this);
            Advertisement.Load(BANNER_AD, this);
            Advertisement.Load(INTERSTITIAL_AD, this);
        }
    }

    public void OnInitializationComplete()
    {
        Debug.Log("Unity Ads initialization complete.");
    }

    public void OnInitializationFailed(UnityAdsInitializationError error, string message)
    {
        Debug.Log($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
    }

    public void ShowBanner()
    {
        if (_isBannerAdLoaded)
            Advertisement.Show(BANNER_AD, this);
    }

    public void HideBanner()
    {
        Advertisement.Banner.Hide();
        Advertisement.Load(BANNER_AD, this);
    }

    public void ShowInterstitialAd()
    {
        if (_isInterstitialAdLoaded)
            Advertisement.Load(INTERSTITIAL_AD, this);
    }

    public void ShowRewardedVideo(UnityAction onWatchedAction)
    {
        if (_isRewardedAdLoaded)
        {
            _onWatchedRewardedVideo = onWatchedAction;
            Advertisement.Show(REWARDED_AD, this);
        }
    }

    public bool IsRewardedVideoReady()
    {
        return _isRewardedAdLoaded;
    }

    public void OnUnityAdsAdLoaded(string placementId)
    {
        switch (placementId)
        {
            case REWARDED_AD:
                _isRewardedAdLoaded = true;
                break;
            case INTERSTITIAL_AD:
                _isInterstitialAdLoaded = true;
                break;
            case BANNER_AD:
                _isBannerAdLoaded = true;
                break;
        }
    }

    public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
    {
        Debug.Log($"Unity Ads failed to load: {placementId} , error {error.ToString()}, message {message}");

    }

    public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
    {
        Debug.Log($"Unity Ads failed to show: {placementId} , error {error.ToString()}, message {message}");
    }

    public void OnUnityAdsShowStart(string placementId)
    {

    }

    public void OnUnityAdsShowClick(string placementId)
    {
    }

    public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
    {
        switch (placementId)
        {
            case REWARDED_AD:
                _onWatchedRewardedVideo?.Invoke();
                _isRewardedAdLoaded = false;
                Advertisement.Load(REWARDED_AD, this);
                break;
            case INTERSTITIAL_AD:
                _isInterstitialAdLoaded = false;
                Advertisement.Load(INTERSTITIAL_AD, this);
                break;
        }
    }

    public void Update()
    {
        TimeSinceLastAd += Time.unscaledDeltaTime;
    }

    public void CheckIfCanShowAd()
    {
        if (TimeSinceLastAd <= ADS_INTERVAL)
        {
            return;
        }

        ShowInterstitialAd();
        TimeSinceLastAd = 0;
    }

    public bool CanShowBannerAd()
    {
        return Time.realtimeSinceStartup > 80;
    }
}
