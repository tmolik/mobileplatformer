using Google.Play.Review;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InAppReviewManager : Singleton<InAppReviewManager>
{
    private static bool _askedForReviewInThisSession = false;

    private const string REVIEW_REQUEST_COUNT_KEY = "revReqCount";

    private ReviewManager _reviewManager;

    // Start is called before the first frame update
    void Start()
    {
        _reviewManager = new ReviewManager();
    }

    public void RequestInAppReview()
    {
        StartCoroutine(InAppReviewRoutine());
    }

    private IEnumerator InAppReviewRoutine()
    {
        var requestFlowOperation = _reviewManager.RequestReviewFlow();
        yield return requestFlowOperation;
        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
        {
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }
         PlayReviewInfo _playReviewInfo = requestFlowOperation.GetResult();

        var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
        yield return launchFlowOperation;
        _playReviewInfo = null; // Reset the object
        _askedForReviewInThisSession = true;
        IncreaseRequestsCount();
        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        {
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }

    }

    private void IncreaseRequestsCount()
    {
        int currentRequestsCount = PlayerPrefs.GetInt(REVIEW_REQUEST_COUNT_KEY, 0);
        PlayerPrefs.SetInt(REVIEW_REQUEST_COUNT_KEY, currentRequestsCount + 1);
    }



    public bool CanShowInAppReview()
    {
        if (GameFilesManager.CurrentGameData == null)
            return false;

        if(GameFilesManager.CurrentGameData.CurrentLevelID < 25)
        {
            //Debug.Log("He didnt even finish quarter of the game, dont ask");
            return false;
        }

        if (Time.realtimeSinceStartup < 5 * 60)
        {
           // Debug.Log("He didnt play long enough in this session");
            return false;
        }

        if (_askedForReviewInThisSession)
        {
            //Debug.Log("He was already asked in this session");
            return false;
        }

        if(PlayerPrefs.GetInt(REVIEW_REQUEST_COUNT_KEY,0)>=2)
        {
            //Debug.Log("He was already asked twice, stop");
            return false;
        }


        return true;
    }
}
