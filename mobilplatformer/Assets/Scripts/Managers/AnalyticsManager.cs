using UnityEngine.Analytics;

public class AnalyticsManager : Singleton<AnalyticsManager>
{
    public void ReportLevelStarted(int levelId)
    {
        AnalyticsEvent.LevelStart(levelId);
    }

    public void ReportLevelFinished(int levelId)
    {
        AnalyticsEvent.LevelComplete(levelId);
    }

    public void ReportLevelFailed(int levelId)
    {
        AnalyticsEvent.LevelFail(levelId);
    }

    public void ReportCustomizationScreenVisit()
    {
        AnalyticsEvent.ScreenVisit("ColorsCustiomization");
    }

}
