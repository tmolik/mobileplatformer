﻿using Molioo.Localization;

[System.Serializable]
public class SettingsData
{
    public ELanguage Language;

    public bool IsMuted = false;

    public int ColorPresetID = 0;
}
