﻿using Molioo.UBB.Levels;
using UnityEngine;

public class StartupSceneController : MonoBehaviour
{
    void Start()
    {
        SettingsManager.Initialize();
        QualitySettings.SetQualityLevel(5);
        GameFilesManager.LoadProgress();
        if (LevelsManager.Instance != null)
            LevelsManager.Instance.LoadMenu();
    }
}
