﻿using Molioo.Achievements;
using UnityEngine;

namespace Molioo.UBB.Levels
{
    public class EndlessLevelController : LevelController
    {
        public static bool ContinueWithWatchPossible = true;

        [HideInInspector]
        public float TimeSpentInEndless = 0;

        [SerializeField]
        private GlobalFloatVariable _endlessTimeVariable = null;

        private bool _playerAlive = true;

        private bool _canCountTime = false;

        public override void Start()
        {
            GameManager.CurrentGameMode = EGameMode.Endless;
            ContinueWithWatchPossible = true;
            TimeSpentInEndless = 0;
            if (_endlessTimeVariable != null)
            {
                _endlessTimeVariable.Value = 0;
            }
        }

        public override void Update()
        {
            if (!_playerAlive || !_canCountTime)
                return;

            TimeSpentInEndless += Time.deltaTime;
            if (_endlessTimeVariable != null)
            {
                _endlessTimeVariable.Value = TimeSpentInEndless;
            }
        }

        public void StartCountingTime()
        {
            _canCountTime = true;
        }

        public override void PlayerDied(PlayerCubeController player)
        {
            _playerAlive = false;
            _playerCubeController = player;
            _canCountTime = false;
            StartCoroutine(PlayerDeathRoutine(player));
        }

        protected override void FinalizePlayerDead()
        {
            CheckEndlessModeAchievements();
            UpdateLeaderboard();
            CheckAdPossibility();
            UiManager.Instance.EndlessSummaryMenu.Show();
        }

        private void CheckEndlessModeAchievements()
        {
            if (_endlessTimeVariable == null)
                return;

            if (AchievementsManager.Instance == null)
                return;

            if (_endlessTimeVariable.Value >= 60)
            {
                AchievementsManager.Instance.UnlockAchievement(EAchievementType.Survive60SecondsInEndless);
            }
            if (_endlessTimeVariable.Value > 5 * 60)
            {
                AchievementsManager.Instance.UnlockAchievement(EAchievementType.Survive5MinInEndless);
            }
        }


        private void UpdateLeaderboard()
        {
            if (_endlessTimeVariable == null)
                return;

            if (SocialPlatformManager.Instance == null)
                return;

            SocialPlatformManager.Instance.UpdateLeaderboard((int)_endlessTimeVariable.Value);
        }

        private void CheckAdPossibility()
        {
            if (AdsManager.Instance != null)
                AdsManager.Instance.CheckIfCanShowAd();
        }

        public void RestartEndless()
        {
            if (_shatteredPlayerObject != null)
                Destroy(_shatteredPlayerObject);

            _playerAlive = true;

            if (_playerCubeController != null)
            {
                _playerCubeController.transform.position = EndlessModeController.Instance.GetClosestLevelPart(_playerCubeController.transform.position).SafeStartTransform.position;
                _playerCubeController.ResetInEndless();
                _playerCubeController.gameObject.SetActive(true);
            }
        }

        public override string GetThisLevelId()
        {
            return "Endless";
        }
    }
}