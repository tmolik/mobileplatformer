﻿using Molioo.Achievements;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Molioo.UBB.Levels
{
    public class LevelController : Singleton<LevelController>
    {
        protected GameObject _shatteredPlayerObject = null;

        protected PlayerCubeController _playerCubeController = null;

        private int _IdOfThisLevel = 0;

        private bool _levelFinished = false;

        public virtual void Start()
        {
            string sceneName = SceneManager.GetActiveScene().name;
            if (sceneName.Contains("Level"))
            {
                string IDstring = sceneName.Split("_"[0])[1];
                _IdOfThisLevel = int.Parse(IDstring);
                LevelsManager.Instance.SetStartedLevel(_IdOfThisLevel);

#if !UNITY_EDITOR
                AnalyticsManager.Instance.ReportLevelStarted(_IdOfThisLevel);
#endif
            }
        }

        public virtual void Update()
        {
            HandleDebugInput();
        }

        private void HandleDebugInput()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                LevelFinished();
            }
        }

        public void LevelFinished()
        {
            if (_levelFinished)
                return;

            _levelFinished = true;
            AudioManager.Instance.PlayLevelFinishedSound();

            if (GameFilesManager.CurrentGameData != null)
            {
                if (GameFilesManager.CurrentGameData.CurrentLevelID < _IdOfThisLevel)
                {
                    GameFilesManager.CurrentGameData.CurrentLevelID = _IdOfThisLevel;
                    GameFilesManager.SaveProgress();
                }
            }

            CheckAchievements();
            CheckAdPossibility();

            if (_IdOfThisLevel == LevelsManager.Instance.GetLastLevelId())
                UiManager.Instance.GameFinishedMenu.Show();
            else
                UiManager.Instance.LevelWonMenu.Show();

            GameManager.CurrentGamePhase = EGamePhase.LevelSummary;

#if !UNITY_EDITOR
                AnalyticsManager.Instance.ReportLevelFinished(_IdOfThisLevel);
#endif
        }

        private void CheckAchievements()
        {
            if (GameFilesManager.CurrentGameData == null)
                return;

            if (AchievementsManager.Instance == null)
                return;

            if (GameFilesManager.CurrentGameData.CurrentLevelID >= 10)
                AchievementsManager.Instance.UnlockAchievement(EAchievementType.Beat10Levels);

            if (GameFilesManager.CurrentGameData.CurrentLevelID >= 25)
                AchievementsManager.Instance.UnlockAchievement(EAchievementType.Beat25Levels);

            if (GameFilesManager.CurrentGameData.CurrentLevelID >= 50)
                AchievementsManager.Instance.UnlockAchievement(EAchievementType.Beat50Levels);

            if (GameFilesManager.CurrentGameData.CurrentLevelID >= 75)
                AchievementsManager.Instance.UnlockAchievement(EAchievementType.Beat75Levels);

            if (GameFilesManager.CurrentGameData.CurrentLevelID >= 100)
                AchievementsManager.Instance.UnlockAchievement(EAchievementType.Beat100Levels);
        }



        public virtual void PlayerDied(PlayerCubeController player)
        {
            StartCoroutine(PlayerDeathRoutine(player));
        }

        protected IEnumerator PlayerDeathRoutine(PlayerCubeController player)
        {
            AudioManager.Instance.PlayPlayerDeathSound();

            ShatteredPlayerCube shatteredPlayer = ObjectPooling.ObjectPooler.Get.GetPooledObject(ObjectPooling.EPoolables.ShatteredPlayer).GetComponent<ShatteredPlayerCube>();
            shatteredPlayer.transform.position = player.transform.position;
            shatteredPlayer.transform.SetParent(transform);
            shatteredPlayer.SetCubeVelocity(player.GetVelocity());

            shatteredPlayer.gameObject.SetActive(true);
            _shatteredPlayerObject = shatteredPlayer.gameObject;

            CameraController.Instance.ScreenShake(2f, 0.3f);
            player.gameObject.SetActive(false);

#if !UNITY_EDITOR
                AnalyticsManager.Instance.ReportLevelFailed(_IdOfThisLevel);
#endif
            yield return new WaitForSeconds(1f);
            FinalizePlayerDead();
        }

        protected virtual void FinalizePlayerDead()
        {
            LevelsManager.Instance.ResetLevel();
        }

        private void CheckAdPossibility()
        {
            if (AdsManager.Instance != null)
                AdsManager.Instance.CheckIfCanShowAd();
        }

        public virtual string GetThisLevelId()
        {
            return _IdOfThisLevel.ToString();
        }
    }
}