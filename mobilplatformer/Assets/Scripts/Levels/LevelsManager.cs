﻿using UnityEngine;

namespace Molioo.UBB.Levels
{
    public class LevelsManager : Singleton<LevelsManager>
    {
        [SerializeField]
        private GlobalIntVariable _levelsCountVariable;

        [SerializeField]
        private string _menuLevelName = "MainMenu";

        [SerializeField]
        private string _endlessLevelName = "EndlessModeScene";

        private const string LEVEL_PREFIX = "Level_";

        private int _currentlyPlayedLevelId;
        private string _currentlyPlayedLevelName;

        public void SelectedLevel(int levelID)
        {
            UiManager.Instance.GameplayUI.Show();
            GameManager.CurrentGamePhase = EGamePhase.Game;
            GameManager.CurrentGameMode = EGameMode.Campaign;

            LoadLevel(LEVEL_PREFIX + levelID, levelID);
        }

        private void LoadLevel(string levelName, int levelID = 0)
        {
            if (string.IsNullOrWhiteSpace(levelName))
                return;

            if (LoadingManager.Instance != null)
                LoadingManager.Instance.LoadScene(levelName);

            _currentlyPlayedLevelId = levelID;
            _currentlyPlayedLevelName = levelName;

            if (UiEndlessTimeTextController.Instance != null)
            {
                UiEndlessTimeTextController.Instance.Clear();
            }
        }

        public void ResetLevel()
        {
            if (GameManager.CurrentGameMode == EGameMode.Campaign)
            {
                if (string.IsNullOrWhiteSpace(_currentlyPlayedLevelName))
                    return;

                LoadLevel(_currentlyPlayedLevelName);
            }
            else
            {
                LoadLevel(_endlessLevelName);
            }
        }

        public void SetStartedLevel(int id)
        {
            _currentlyPlayedLevelId = id;
            _currentlyPlayedLevelName = LEVEL_PREFIX + id;
        }

        public int GetLastLevelId()
        {
            return _levelsCountVariable.Value;
        }

        public void LoadNextLevel()
        {
            int nextLevelId = _currentlyPlayedLevelId + 1;

            LoadLevel(LEVEL_PREFIX + nextLevelId);
            GameManager.CurrentGamePhase = EGamePhase.Game;
            GameManager.CurrentGameMode = EGameMode.Campaign;
            return;
        }

        public void LoadMenu()
        {
            if (!string.IsNullOrWhiteSpace(_menuLevelName))
            {
                if (LoadingManager.Instance != null)
                    LoadingManager.Instance.LoadScene(_menuLevelName);

                UiManager.Instance.GameplayUI.Hide();
                GameManager.CurrentGamePhase = EGamePhase.Menu;
                _currentlyPlayedLevelId = 0;
            }
        }

        public void LoadEndlessLevel()
        {
            if (!string.IsNullOrWhiteSpace(_endlessLevelName))
            {
                if (LoadingManager.Instance != null)
                    LoadingManager.Instance.LoadScene(_endlessLevelName);

                UiManager.Instance.GameplayUI.Show();
                GameManager.CurrentGamePhase = EGamePhase.Game;
                GameManager.CurrentGameMode = EGameMode.Endless;
            }
        }
    }
}