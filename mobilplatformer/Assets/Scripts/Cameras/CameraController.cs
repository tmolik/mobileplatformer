﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : Singleton<CameraController>
{
    [SerializeField]
    private Transform _targetTransform = null;

    [SerializeField]
    private Vector2 _offset = new Vector2(0, 0);

    [SerializeField]
    private float _followSpeed = 5f;

    private Camera _camera;

    // Camera shake
    private float _shakeTime = 0;
    private float _shakePower = 1f;
    private float _decreaseFactor = 1.0f;
    private Vector3 _shakeBonus;

    private void Start()
    {
        SetupCameraSettings();
        GameplaySettingsManager.Instance.OnGameSettingsChanged += SetupCameraSettings;
    }

    private void OnDestroy()
    {
        GameplaySettingsManager.Instance.OnGameSettingsChanged -= SetupCameraSettings;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        CalculateShakeBonus();
        MoveCamera();
    }

    private void MoveCamera()
    {
        if (_targetTransform == null)
            return;

        transform.position = Vector3.Lerp(transform.position, new Vector3(_targetTransform.position.x + _offset.x + _shakeBonus.x, _targetTransform.position.y + _offset.y + _shakeBonus.y, transform.position.z), Time.deltaTime * _followSpeed);
    }

    public void SetupCameraSettings()
    {
        _camera = GetComponent<Camera>();
        _camera.fieldOfView = GameplaySettingsManager.Instance.CurrentGameSettings.CameraFoV;
        transform.position = new Vector3(transform.position.x, transform.position.y,GameplaySettingsManager.Instance.CurrentGameSettings.CameraZPosition);
    }
    
    public void InstantyGoToTargetPosition()
    {
        transform.position = new Vector3(_targetTransform.position.x + _offset.x + _shakeBonus.x, _targetTransform.position.y + _offset.y + _shakeBonus.y, transform.position.z);
    }

    public void ScreenShake(float shakePower, float shakeTime)
    {
        _shakeTime = shakeTime;
        _shakePower = shakePower;
    }

    private void CalculateShakeBonus()
    {
        if (_shakeTime > 0)
        {
            _shakeBonus = Random.insideUnitSphere * _shakePower;
            _shakeBonus = new Vector3(_shakeBonus.x, _shakeBonus.y, _shakeBonus.z);
            _shakeTime -= Time.deltaTime * _decreaseFactor;
        }
        else
        {
            _shakeBonus = Vector3.zero;
        }
    }
}
