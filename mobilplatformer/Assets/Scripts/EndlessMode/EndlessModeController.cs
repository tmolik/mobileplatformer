﻿using System.Collections.Generic;
using UnityEngine;
using Molioo.ObjectPooling;

public class EndlessModeController : Singleton<EndlessModeController>
{
    public PlayerCubeController Player = null;

    [SerializeField]
    private EndlessModeLevelPart _lastLevelPart = null;

    [SerializeField]
    private EndlessModePoolablesList _endlessModePoolables = null;

    [SerializeField]
    private GlobalFloatVariable _timeInEndless = null;

    [SerializeField]
    private AnimationCurve _easyAnimationCurve;

    [SerializeField]
    private AnimationCurve _mediumAnimationCurve;

    [SerializeField]
    private AnimationCurve _hardAnimationCurve;

    private List<GameObject> _spawnedLevelParts = new List<GameObject>();


    public void Update()
    {
        CheckIfShouldSpawnNewLevelPart();
    }

    private void CheckIfShouldSpawnNewLevelPart()
    {
        if (Mathf.Abs(Player.transform.position.x - _lastLevelPart.EndOfPartTransforn.position.x) < 50f)
        {
            SpawnRandomLevelPart();
        }
    }

    public void ClearLevelParts()
    {
        foreach (GameObject levelPartGameObject in _spawnedLevelParts)
        {
            if (levelPartGameObject.activeSelf)
                levelPartGameObject.SetActive(false);
        }
    }

    private void SpawnRandomLevelPart()
    {
        if (_timeInEndless == null)
            return;

        float valEasy = _easyAnimationCurve.Evaluate(_timeInEndless.Value / 100);
        float valMedium = _mediumAnimationCurve.Evaluate(_timeInEndless.Value / 100);
        float valHard = _hardAnimationCurve.Evaluate(_timeInEndless.Value / 100);

        Vector3 chances = new Vector3(valEasy, valMedium, valHard);
        chances *= 1000;
        chances.Normalize();
        chances *= 100;

        GameObject newPart = ObjectPooler.Get.GetPooledObject(_endlessModePoolables.EasyPoolables.GetRandom());

        int max = (int)(chances.x + chances.y + chances.z);
        int random = Random.Range(0, max);

        if (random <= chances.x)
        {
            newPart = ObjectPooler.Get.GetPooledObject(_endlessModePoolables.EasyPoolables.GetRandom());
        }
        else if (random <= chances.x + chances.y)
        {
            newPart = ObjectPooler.Get.GetPooledObject(_endlessModePoolables.MediumPoolables.GetRandom());
        }
        else
        {
            newPart = ObjectPooler.Get.GetPooledObject(_endlessModePoolables.HardPoolables.GetRandom());
        }

        SetNewLevelPart(newPart);
    }

    private void SetNewLevelPart(GameObject newPart)
    {
        if (newPart == null)
            return;

        newPart.transform.position = _lastLevelPart.EndOfPartTransforn.position;
        EndlessModeLevelPart endlessPart = newPart.GetComponent<EndlessModeLevelPart>();
        _lastLevelPart = endlessPart;
        newPart.gameObject.SetActive(true);
        endlessPart.ColliderMergerComponent.TryMerge();

        if (!_spawnedLevelParts.Contains(newPart))
            _spawnedLevelParts.Add(newPart);
    }

    public void LevelPartDeactivated(GameObject levelPartGameObject)
    {
        if (_spawnedLevelParts.Contains(levelPartGameObject))
            _spawnedLevelParts.Remove(levelPartGameObject);
    }

    public EndlessModeLevelPart GetClosestLevelPart(Vector3 position)
    {
        Vector3 closestPosition = new Vector3(1000, 1000, 1000);
        float closestPositionDistance = 100000f;
        EndlessModeLevelPart closestLevelPart = null;
        foreach(GameObject levelPartGameObject in _spawnedLevelParts)
        {
            if(levelPartGameObject.TryGetComponent<EndlessModeLevelPart>(out EndlessModeLevelPart levelPart))
            {
                float distance = Vector3.Distance(position, levelPart.SafeStartTransform.position);
                if(distance<closestPositionDistance)
                {
                    closestPositionDistance = distance;
                    closestPosition = levelPart.SafeStartTransform.position;
                    closestLevelPart = levelPart;
                }
            }
        }

        return closestLevelPart;
    }
}
