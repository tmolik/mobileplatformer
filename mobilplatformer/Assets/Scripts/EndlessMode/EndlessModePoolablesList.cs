﻿using Molioo.ObjectPooling;
using System.Collections.Generic;
using UnityEngine;


    [CreateAssetMenu(fileName = "EndlessPoolableList", menuName = "Settings/Endless Poolable List")]
public class EndlessModePoolablesList : ScriptableObject
{
    public List<EPoolables> EasyPoolables = new List<EPoolables>();
    public List<EPoolables> MediumPoolables = new List<EPoolables>();
    public List<EPoolables> HardPoolables = new List<EPoolables>();
}
