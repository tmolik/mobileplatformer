﻿using UnityEngine;

public class EndlessModeLevelPart : MonoBehaviour
{
    public Transform EndOfPartTransforn = null;

    public Transform SafeStartTransform = null;

    public ColliderMerger ColliderMergerComponent = null;

    public void Update()
    {
        if (EndlessModeController.Instance.Player.transform.position.x - transform.position.x > 150f)
        {
            DisableLevelPart();
        }
    }

    private void DisableLevelPart()
    {
        EndlessModeController.Instance.LevelPartDeactivated(gameObject);
        gameObject.SetActive(false);
    }
}
