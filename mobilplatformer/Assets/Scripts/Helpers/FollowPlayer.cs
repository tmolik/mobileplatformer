﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField]
    private Transform _targetTransform = null;

    [SerializeField]
    private bool _followXPosition = false;
    [SerializeField]
    private bool _followYPosition = false;
    [SerializeField]
    private bool _followZPosition = false;

    // Update is called once per frame
    void Update()
    {
        Vector3 newPositon = Vector3.zero;
        newPositon.x = _followXPosition ? _targetTransform.position.x : transform.position.x;
        newPositon.y = _followYPosition ? _targetTransform.position.y : transform.position.y;
        newPositon.z = _followZPosition ? _targetTransform.position.z : transform.position.z;
        transform.position = newPositon;
    }
}
