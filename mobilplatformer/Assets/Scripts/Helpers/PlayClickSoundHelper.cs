using UnityEngine;

public class PlayClickSoundHelper : MonoBehaviour
{
    public void PlaySound()
    {
        if(AudioManager.Instance!=null)
        {
            AudioManager.Instance.PlayUiClickSound();
        }
    }
}
