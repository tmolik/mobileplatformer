﻿using UnityEngine;

public class MoveObjectsToPositionHelper : MonoBehaviour
{
    [SerializeField]
    private Vector3 _newPosition = Vector3.zero;

    [ContextMenu("Move object")]
    public void Move()
    {
        Vector3 positionDiff = _newPosition - transform.position;
        transform.position = _newPosition;
        foreach(Transform t in transform)
        {
            t.transform.position -= positionDiff;
        }
        DestroyImmediate(this);
    }
}
