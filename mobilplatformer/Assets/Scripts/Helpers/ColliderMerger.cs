using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// What happens in this class, stays in this class.
/// 
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class ColliderMerger : MonoBehaviour
{
    [SerializeField]
    private BoxCollider _thisBoxCollider;

    [SerializeField]
    private LayerMask _layerMask;

    private List<Collider> _mergedColliders = new List<Collider>();
    private Collider _newMergedCollider;
    private BoxCollider _colliderOnLeft;

    public void OnDisable()
    {
        foreach (Collider coll in _mergedColliders)
        {
            if (coll != null)
                coll.enabled = true;
        }
        _mergedColliders = new List<Collider>();

        if (_newMergedCollider != null)
        {
            Destroy(_newMergedCollider.gameObject);
            _newMergedCollider = null;
        }
        _colliderOnLeft = null;
    }

    [ContextMenu("Try merge")]
    public void TryMerge()
    {
        _colliderOnLeft = FindColliderOnTheLeft();
        if (_colliderOnLeft != null)
        {
            MergeWithCollider(_colliderOnLeft);
        }
    }

    [ContextMenu("Check")]
    private BoxCollider FindColliderOnTheLeft()
    {
        if (_thisBoxCollider == null)
            _thisBoxCollider = GetComponent<BoxCollider>();
        Vector3 colliderCenter = transform.position + Vector3.right * (transform.localScale.x * _thisBoxCollider.center.x);
        float distance = (transform.localScale.x * _thisBoxCollider.size.x) / 2;
        RaycastHit hit;
        if (Physics.Raycast(colliderCenter, Vector3.left, out hit, distance + 0.1f, _layerMask, QueryTriggerInteraction.Ignore))
        {
            if (hit.collider is BoxCollider)
            {
                //Debug.Log("Uderzy�o w " + hit.collider.name + " from " + gameObject.name, hit.collider.gameObject);
                return hit.collider as BoxCollider;
            }
        }

        return null;
    }

    private void MergeWithCollider(BoxCollider colliderOnLeft)
    {
        Vector3 secondColliderCenter = colliderOnLeft.transform.position + Vector3.right * (colliderOnLeft.transform.localScale.x * colliderOnLeft.center.x);
        float secondRealXSize = colliderOnLeft.transform.localScale.x * colliderOnLeft.size.x;
        //Debug.Log("second collider center " + secondColliderCenter + " and x size " + secondRealXSize);

        Vector3 firstColliderCenter = transform.position + Vector3.right * (transform.localScale.x * _thisBoxCollider.center.x);
        float firstRealXSize = transform.localScale.x * _thisBoxCollider.size.x;
        //Debug.Log("first collider center " + firstColliderCenter + " and x size " + firstRealXSize);

        float leftSideX = secondColliderCenter.x - secondRealXSize / 2;

        float realNewColliderXSize = secondRealXSize + firstRealXSize;
        Vector3 realNewColliderCenter = new Vector3(leftSideX + realNewColliderXSize / 2, secondColliderCenter.y, secondColliderCenter.z); //  (secondColliderCenter + firstColliderCenter) / 2;
        //Debug.Log("It should have center in " + realNewColliderCenter + " and x size " + realNewColliderXSize);

        GameObject colliderGameObject = new GameObject("MergedCollider");
        colliderGameObject.transform.position = realNewColliderCenter;
        colliderGameObject.transform.localScale = new Vector3(1, 1, 2);
        BoxCollider boxCollider = colliderGameObject.AddComponent<BoxCollider>();
        boxCollider.size = new Vector3(realNewColliderXSize, 1, 1);
        //colliderGameObject.transform.SetParent(transform);

        _newMergedCollider = boxCollider;
        _newMergedCollider.enabled = true;

        colliderOnLeft.enabled = false;
        _thisBoxCollider.enabled = false;

        //Debug.Log("Disabling colliders on " + colliderOnLeft.name + " and " + _thisBoxCollider);
        _mergedColliders.Add(colliderOnLeft);
        _mergedColliders.Add(_thisBoxCollider);
    }
}
