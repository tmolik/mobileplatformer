using System.Collections.Generic;
using UnityEngine;

public class TimedJumpsHelper : MonoBehaviour
{
    [SerializeField]
    private List<float> _registeredJumps = new List<float>();

    [SerializeField]
    private bool _isRecording = false;


    // Start is called before the first frame update
    void Start()
    {
        if(!_isRecording)
        {
            foreach(float jumpTime in _registeredJumps)
            {
                Invoke("SimulateJump", jumpTime);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(_isRecording)
        {
            if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                _registeredJumps.Add(Time.time);
            }
        }
    }

    private void SimulateJump()
    {
        PlayerInputController.OverrideWantToJump = true;
    }

}
