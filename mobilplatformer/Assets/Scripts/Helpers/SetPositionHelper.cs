using System.Collections.Generic;
using UnityEngine;

public class SetPositionHelper : MonoBehaviour
{
    [SerializeField]
    private List<PositionHelper> _positions = new List<PositionHelper>();

    public void SetPosition(int index)
    {
        foreach(PositionHelper posHelper in _positions)
        {
            if(posHelper.Index == index)
            {
                transform.position = posHelper.Position;
            }
        }
    }
}

[System.Serializable]
public class PositionHelper
{
    public Vector3 Position;
    public int Index;
}
