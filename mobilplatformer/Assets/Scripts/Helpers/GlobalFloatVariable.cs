﻿using UnityEngine;

[CreateAssetMenu(fileName = "GlobaFloatVariable", menuName = "Global Variables/Global Float Variable")]
public class GlobalFloatVariable : ScriptableObject
{
    public float Value = 0;
}

