﻿using UnityEngine;

[CreateAssetMenu(fileName = "GlobaIntVariable", menuName = "Global Variables/Global Int Variable")]
public class GlobalIntVariable : ScriptableObject
{
    public int Value = 0;
}
