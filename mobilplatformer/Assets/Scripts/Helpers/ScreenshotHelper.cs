﻿using UnityEngine;

public class ScreenshotHelper : MonoBehaviour
{
    private int index = 0;

    // Update is called once per frame
    void Update()
    {
        HandleScreenshotInput();
    }

    private void HandleScreenshotInput()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ScreenCapture.CaptureScreenshot("D://Screenshot" + index + ".png");
            index += 1;
        }
    }

    
}
