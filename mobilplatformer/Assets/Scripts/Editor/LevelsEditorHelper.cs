﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LevelsEditorHelper : MonoBehaviour
{
    [MenuItem("Assets/Levels/Increase level number")]
    public static void IncreaseLevelNumbers()
    {
        for(int i=Selection.objects.Length-1; i>=0;i--)
        {
            UnityEngine.Object selectedObject = Selection.objects[i];
            if (selectedObject.GetType() == typeof(SceneAsset))
            {
                Debug.Log("Znalazłem asset sceny");
                if (selectedObject.name.Contains("Level"))
                {
                    string IDstring = selectedObject.name.Split("_"[0])[1];
                    int idOfThisLevel = int.Parse(IDstring);

                    string path = AssetDatabase.GetAssetPath(selectedObject);
                    Debug.Log("Path to " + path);
                    string newPath = path.Replace(IDstring, (idOfThisLevel + 1).ToString());
                    Debug.Log("NewPath to " + newPath);
                    AssetDatabase.RenameAsset(path, "Level_" + (idOfThisLevel + 1).ToString());
                    //EditorUtility.SetDirty(selectedObject);
                    //Debug.Log("Zmieniam nazwę na Level_" + (idOfThisLevel + 1));

                }
            }
        }

        //foreach (UnityEngine.Object selectedObject in Selection.objects)
        //{
        //    if (selectedObject.GetType() == typeof(SceneAsset))
        //    {
        //        Debug.Log("Znalazłem asset sceny");
        //        if (selectedObject.name.Contains("Level"))
        //        {
        //            string IDstring = selectedObject.name.Split("_"[0])[1];
        //            int idOfThisLevel = int.Parse(IDstring);

        //            string path = AssetDatabase.GetAssetPath(selectedObject);
        //            Debug.Log("Path to " + path);
        //            string newPath = path.Replace(IDstring, (idOfThisLevel + 1).ToString());
        //            Debug.Log("NewPath to " + newPath);
        //            AssetDatabase.RenameAsset(path, "Level_"+ (idOfThisLevel + 1).ToString());
        //            //EditorUtility.SetDirty(selectedObject);
        //            //Debug.Log("Zmieniam nazwę na Level_" + (idOfThisLevel + 1));

        //        }
        //    }
        //}
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}
