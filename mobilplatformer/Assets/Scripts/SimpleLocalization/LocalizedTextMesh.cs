﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Molioo.Localization
{
    [RequireComponent(typeof(TMPro.TextMeshProUGUI))]
    public class LocalizedTextMesh : MonoBehaviour
    {
        [SerializeField]
        private string _key = "";

        private TextMeshProUGUI _localizedText;

        private void Awake()
        {
            if (_localizedText == null)
                _localizedText = GetComponent<TextMeshProUGUI>();
            LocalizationManager.LanguageChanged += OnLanguageChanged;
        }

        private void OnLanguageChanged(ELanguage obj)
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(SetCorrectTextWhenLocalizationManagerIsReady());
        }

        private void OnEnable()
        {
            StartCoroutine(SetCorrectTextWhenLocalizationManagerIsReady());
        }


        private IEnumerator SetCorrectTextWhenLocalizationManagerIsReady()
        {
            if (string.IsNullOrWhiteSpace(_key))
                yield break;

            if (_localizedText == null)
                _localizedText = GetComponent<TextMeshProUGUI>();

            while (LocalizationManager.Instance == null)
                yield return null;

            _localizedText.text = LocalizationManager.Instance.GetLocalizedText(_key);
        }
    }
}