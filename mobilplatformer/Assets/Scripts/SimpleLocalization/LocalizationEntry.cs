﻿using System.Collections.Generic;

namespace Molioo.Localization
{
    public class LocalizationEntry
    {
        public string Key = "";
        public Dictionary<ELanguage, string> _translations = new Dictionary<ELanguage, string>();

        public string GetLocalization(ELanguage language)
        {
            return _translations.ContainsKey(language) ? _translations[language] : ""; 
        }

        public void AddNewTranslation(ELanguage language, string translation)
        {
            if (_translations.ContainsKey(language))
                return;

            _translations.Add(language, translation);
        }
    }
}