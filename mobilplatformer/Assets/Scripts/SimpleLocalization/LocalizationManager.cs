﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Molioo.Localization
{
    public class LocalizationManager : Singleton<LocalizationManager>
    {
        public static event Action<ELanguage> LanguageChanged;

        [SerializeField]
        private TextAsset _localizationCSVAsset = null;

        private Dictionary<string, ELanguage> _languagesHelper = new Dictionary<string, ELanguage>();
        private Dictionary<string, LocalizationEntry> _localizations = new Dictionary<string, LocalizationEntry>();

        private ELanguage _currentLanguage = ELanguage.Polski;

        public override void Awake()
        {
            base.Awake();
            InitializeLocalizationManager();
        }

        private void InitializeLocalizationManager()
        {
            _languagesHelper.Add("EN", ELanguage.English);
            _languagesHelper.Add("PL", ELanguage.Polski);

            LoadLocalizationsFromFile();
            _currentLanguage = ELanguage.English;
            if (LanguageChanged != null)
                LanguageChanged.Invoke(_currentLanguage);
        }

        public void Update()
        {
            HandleDebugInput();
        }

        private void HandleDebugInput()
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                SetNextLanguage();
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                SetPreviousLanguage();
            }
        }

        public void SetNextLanguage()
        {
            int currentLanguageIndex = (int)_currentLanguage;
            currentLanguageIndex++;
            if (currentLanguageIndex >= System.Enum.GetValues(typeof(ELanguage)).Length)
                currentLanguageIndex = 0;

            _currentLanguage = (ELanguage)currentLanguageIndex;
            LanguageChanged.Invoke(_currentLanguage);
        }

        public void SetPreviousLanguage()
        {
            int currentLanguageIndex = (int)_currentLanguage;
            currentLanguageIndex--;
            if (currentLanguageIndex < 0)
                currentLanguageIndex = System.Enum.GetValues(typeof(ELanguage)).Length - 1;

            _currentLanguage = (ELanguage)currentLanguageIndex;
            LanguageChanged.Invoke(_currentLanguage);
        }

        public void SetSpecificLanguage(ELanguage languageToSet)
        {
            _currentLanguage = languageToSet;
            if (LanguageChanged != null)
                LanguageChanged.Invoke(_currentLanguage);
        }

        public ELanguage GetLanguage()
        {
            return _currentLanguage;
        }

        public string GetLocalizedText(string key)
        {
            key = key.Trim();
            if (_localizations.ContainsKey(key))
            {
                return _localizations[key].GetLocalization(_currentLanguage);
            }
            else
            {
                return "";
            }

        }
        private void LoadLocalizationsFromFile()
        {
            if (_localizationCSVAsset == null)
            {
                Debug.LogError("Localization asset is empty!");
                return;
            }


            List<string> linesOfText = new List<string>();
            linesOfText.AddRange(_localizationCSVAsset.text.Split('\n'));

            List<ELanguage> languagesOrder = GetLanguageOrder(linesOfText[0]);

            for (int i = 1; i < linesOfText.Count; i++)
            {
                List<string> entry = new List<string>();
                entry.AddRange(linesOfText[i].Split(';'));

                LocalizationEntry newEntry = new LocalizationEntry();

                for (int j = 1; j < entry.Count; j++)
                {
                    newEntry.AddNewTranslation(languagesOrder[j - 1], entry[j]);
                }

                _localizations.Add(entry[0], newEntry);
            }
        }

        private List<ELanguage> GetLanguageOrder(string languagesLine)
        {
            List<ELanguage> languagesOrder = new List<ELanguage>();
            string[] languages = languagesLine.Trim().Split(';');

            for (int i = 1; i < languages.Length; i++)
            {
                if (_languagesHelper.ContainsKey(languages[i]))
                {
                    languagesOrder.Add(_languagesHelper[languages[i]]);
                }
            }
            return languagesOrder;
        }
    }
}
