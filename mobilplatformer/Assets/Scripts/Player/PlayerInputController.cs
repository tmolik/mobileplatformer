﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInputController
{
    public static bool OverrideWantToJump = false;

    private const KeyCode JUMP_KEYCODE = KeyCode.Space;

    public bool WantsToJump()
    {
        if (Time.timeScale == 0)
            return false;

        if (OverrideWantToJump)
        {
            OverrideWantToJump = false;
            return true;
        }

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        return DidTapInThisFrame();
#else
        return Input.GetKeyDown(JUMP_KEYCODE) || Input.GetMouseButtonDown(0);
#endif
    }

    private bool DidTapInThisFrame()
    {
        if (Input.touchCount == 0)
            return false;

        for (int i = 0; i <= Input.touchCount - 1; i++)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId))
                    continue;

                return true;
            }
        }

        return false;
    }

}
