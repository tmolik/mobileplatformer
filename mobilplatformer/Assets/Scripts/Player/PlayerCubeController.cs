﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerCubeController : MonoBehaviour
{
    public LayerMask GroundLayerMask;

    [HideInInspector]
    public TrailRenderer TrailRenderer;

    [SerializeField]
    private float _speed = 4f;

    private float _speedMultiplier = 1f;

    public float SpeedMultiplier { get => _speedMultiplier; set => _speedMultiplier = value; }

    [SerializeField]
    private float _jumpPower = 4f;

    [Header("Events")]
    [SerializeField]
    private UnityEvent _onPlayerStartedMoving = new UnityEvent();

    private PlayerInputController _inputController;
    private PlayerAudioController _audioController;

    private bool _isGrounded = false;
    private bool _startedMoving = false;

    private Rigidbody _playerRigidbody = null;

    private Vector3 _currentDirection = Vector3.right;


    public void Awake()
    {
        InitializeComponents();
    }

    public void Start()
    {
        SetupPlayerSettings();
        GameplaySettingsManager.Instance.OnGameSettingsChanged += SetupPlayerSettings;
    }

    public void OnDestroy()
    {
        GameplaySettingsManager.Instance.OnGameSettingsChanged -= SetupPlayerSettings;
    }

    private void InitializeComponents()
    {
        _playerRigidbody = GetComponent<Rigidbody>();
        _inputController = new PlayerInputController();
        _audioController = GetComponent<PlayerAudioController>();
        TrailRenderer = GetComponentInChildren<TrailRenderer>();
    }

    private void SetupPlayerSettings()
    {
        _speed = GameplaySettingsManager.Instance.CurrentGameSettings.PlayerMovementSpeed;
        _jumpPower = GameplaySettingsManager.Instance.CurrentGameSettings.PlayerJumpPower;
    }

    private void Update()
    {
        SideRaycasts();
        GroundRaycast();
        HandleDebugInput();
        HandleJumping();
    }

    private void HandleJumping()
    {
        if (_startedMoving)
        {
            if (_inputController.WantsToJump() && _isGrounded)
                Jump();
        }
        else
        {
            if (_inputController.WantsToJump())
            {
                StartMoving();
            }
        }
    }

    private void HandleDebugInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
            ChangeDirection();

        if (Input.GetKeyDown(KeyCode.LeftAlt))
            FixStuckPlayer();
    }

    private void StartMoving()
    {
        _startedMoving = true;
        if (_onPlayerStartedMoving != null)
            _onPlayerStartedMoving.Invoke();
    }

    public void StopMoving()
    {
        _startedMoving = false;
        _playerRigidbody.velocity = Vector3.zero;
    }

    private void FixedUpdate()
    {
        if (_playerRigidbody == null || !_startedMoving)
            return;

        _playerRigidbody.velocity = new Vector3(_currentDirection.x * _speed * _speedMultiplier, _playerRigidbody.velocity.y, _playerRigidbody.velocity.z);
    }

    public void ChangeDirection()
    {
        _currentDirection = -1 * _currentDirection;
    }

    public Vector3 GetVelocity()
    {
        return _playerRigidbody.velocity;
    }

    public void Bounce(float minimumYVelocity)
    {
        if (_playerRigidbody == null)
            return;

        float newVelocity = Mathf.Max(_playerRigidbody.velocity.y * -1, minimumYVelocity);
        if (_playerRigidbody.velocity.y < 0)
            _playerRigidbody.velocity = new Vector3(_currentDirection.x * _speed, newVelocity, _playerRigidbody.velocity.z);
    }

    private void Jump()
    {
        if (_playerRigidbody != null)
        {
            _playerRigidbody.velocity = new Vector3(_playerRigidbody.velocity.x, 0, _playerRigidbody.velocity.z);
            _playerRigidbody.AddForce(new Vector3(0, _jumpPower, 0), ForceMode.Impulse);
        }
        AudioManager.Instance.PlayPlayerJumpSound();
    }

    private void GroundRaycast()
    {
        RaycastHit hit;
        bool hitDetect = Physics.BoxCast(transform.position, new Vector3(0.5f, 0.1f, 0.5f), transform.up * -1, out hit, transform.rotation, 0.6f, GroundLayerMask, QueryTriggerInteraction.Ignore);
        if (hitDetect)
        {
            _isGrounded = true;
        }
        else
        {
            _isGrounded = false;
        }
    }

    private void SideRaycasts()
    {
        Vector3 yVelocityBonus = Vector3.zero;
        if (_playerRigidbody.velocity.y < -1)
        {
            yVelocityBonus = new Vector3(0, 0.15f, 0);
        }
        RaycastHit hit;
        bool hitDetect = Physics.BoxCast(transform.position + yVelocityBonus, new Vector3(0.1f, 0.49f, 0.5f), _currentDirection, out hit, transform.rotation, 0.55f, GroundLayerMask, QueryTriggerInteraction.Ignore);
        if (hitDetect)
        {

            //This is an ugly way to do that, its a fix for changing direction on edge of dissappearing floors
            if (hit.collider.TryGetComponent<DissapearingObject>(out DissapearingObject disObject))
            {
                Vector3 hitDiff = hit.point - transform.position;
                if (Mathf.Abs(hitDiff.y) >= 0.4f)
                {
                    transform.position += Vector3.up * 0.1f;
                    return;
                }

            }
            ChangeDirection();
        }
    }

    private bool _fixing = false;

    [ContextMenu("Fix")]
    private void FixStuckPlayer()
    {
        if (!_startedMoving && _fixing)
            return;
        if (_playerRigidbody.velocity != Vector3.zero)
            return;

        _fixing = true;
        StartCoroutine(StuckFixRoutine());
    }

    private IEnumerator StuckFixRoutine()
    {
        _playerRigidbody.GetComponent<Collider>().enabled = false;
        yield return new WaitForEndOfFrame();
        _playerRigidbody.GetComponent<Collider>().enabled = true;
        _fixing = false;
    }

    public void ResetInEndless()
    {
        _startedMoving = false;
        _playerRigidbody.velocity = Vector3.zero;
        _currentDirection = Vector3.right;
    }

}
