﻿using UnityEngine;

public class ShatteredPlayerCube : MonoBehaviour
{
    public void SetCubeVelocity(Vector3 velocity)
    {
        foreach(Rigidbody rbody in GetComponentsInChildren<Rigidbody>())
        {
            rbody.velocity = velocity + Random.insideUnitSphere;
        }
    }
}
