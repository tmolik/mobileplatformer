﻿using UnityEngine;

[System.Serializable]
public class PlayerAudioController : MonoBehaviour
{
    [SerializeField]
    private AudioClip _jumpAudioClip = null;

    public void PlayJumpAudio()
    {
        if (_jumpAudioClip != null)
            AudioManager.Instance.PlayAudio2D(_jumpAudioClip);
    }
}
