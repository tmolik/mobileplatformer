﻿using UnityEngine;

namespace Molioo.UBB.Tutorial
{
    public class TutorialInvokerTrigger : MonoBehaviour
    {
        [SerializeField]
        private ETutorialPhase _tutorialToInvoke = ETutorialPhase.TapToJump;

        public void OnTriggerEnter(Collider other)
        {
            if (other.attachedRigidbody == null)
                return;

            if(other.attachedRigidbody.TryGetComponent(out PlayerCubeController controller))
            {
                TutorialManager.Instance.ShowTutorialObject(_tutorialToInvoke);
                gameObject.SetActive(false);
            }
        }
    }
}