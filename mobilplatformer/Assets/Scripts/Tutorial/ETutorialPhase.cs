﻿namespace Molioo.UBB.Tutorial
{
    public enum ETutorialPhase
    {
        TapToJump,
        JumpOnTrampoline,
        GoThroughPortal
    }
}