﻿using System.Collections.Generic;
using UnityEngine;

namespace Molioo.UBB.Tutorial
{
    public class TutorialManager : Singleton<TutorialManager>
    {
        [SerializeField]
        private List<TutorialPhaseGameObjectHelper> _tutorialGameobjects = new List<TutorialPhaseGameObjectHelper>();

        public void ShowTutorialObject(ETutorialPhase tutorialPhase)
        {
            if (WasTutorialSeen(tutorialPhase))
                return;

            for (int i = 0; i <= _tutorialGameobjects.Count - 1; i++)
            {
                if (_tutorialGameobjects[i].TutorialPhase == tutorialPhase)
                {
                    _tutorialGameobjects[i].TutorialGameObject.SetActive(true);
                    Time.timeScale = 0;
                    SaveTutorialSeen(tutorialPhase);
                    return;
                }
            }
        }

        private void SaveTutorialSeen(ETutorialPhase tutorial)
        {
            if (GameFilesManager.CurrentGameData != null)
            {
                if (GameFilesManager.CurrentGameData.TutorialSaves.Count == 0)
                {
                    GameFilesManager.CurrentGameData.PopulateNewTutorialSaves();
                }

                GameFilesManager.CurrentGameData.ChangeTutorialSave(tutorial, true);
                GameFilesManager.SaveProgress();
            }
        }

        private bool WasTutorialSeen(ETutorialPhase tutorial)
        {
            if (GameFilesManager.CurrentGameData != null)
            {
                return GameFilesManager.CurrentGameData.WasTutorialSeen(tutorial);
            }

            return false;
        }

        [ContextMenu("Reset Save")]
        public void ResetTutorialSave()
        {
            if (GameFilesManager.CurrentGameData != null)
            {
                GameFilesManager.CurrentGameData.PopulateNewTutorialSaves();
                GameFilesManager.SaveProgress();
            }
        }

        public void TutorialClosed()
        {
            Time.timeScale = 1;
        }
    }

    [System.Serializable]
    public class TutorialPhaseGameObjectHelper
    {
        public ETutorialPhase TutorialPhase = ETutorialPhase.TapToJump;
        public GameObject TutorialGameObject = null;
    }

    [System.Serializable]
    public class TutorialPhaseSaveHelper
    {
        public ETutorialPhase TutorialPhase = ETutorialPhase.TapToJump;
        public bool WasSeen = false;

        public TutorialPhaseSaveHelper(ETutorialPhase tutorialPhase, bool wasSeen)
        {
            TutorialPhase = tutorialPhase;
            WasSeen = wasSeen;
        }
    }

}