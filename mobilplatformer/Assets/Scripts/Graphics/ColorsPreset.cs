using UnityEngine;

[CreateAssetMenu(fileName = "ColorsPreset",menuName = "Presets/Colors Preset")]
public class ColorsPreset : ScriptableObject
{
    public int ColorPresetID = 0;
    public Color BaseLevelObjectsColor = new Color(202 / 255f, 202 / 255f, 202 / 255f, 1); 
    public Color ObstaclesColor = new Color(1, 0, 0, 1);
    public Color BackgroundColor = new Color(0.1929067f, 0.7011893f, 0.8018868f, 1);
    public Color PlayerColor = new Color(0.9716981f, 0.8296102f, 0.9085131f, 1);
}
