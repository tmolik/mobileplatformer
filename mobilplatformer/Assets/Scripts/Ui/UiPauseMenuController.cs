﻿using Molioo.Localization;
using Molioo.UBB.Levels;
using TMPro;
using UnityEngine;

public class UiPauseMenuController : UiBaseMenuController
{
    [SerializeField]
    private TextMeshProUGUI _levelTextMesh;

    public void OnClickContinue()
    {
        Hide();
    }

    public void OnClickReturnToMenu()
    {
        if (GameManager.CurrentGameMode == EGameMode.Endless && EndlessModeController.Instance != null)
        {
            EndlessModeController.Instance.ClearLevelParts();
        }

        LevelsManager.Instance.LoadMenu();

        Hide();
    }

    public void OnClickMute()
    {
        SettingsManager.CurrentSettings.IsMuted = !SettingsManager.CurrentSettings.IsMuted;
        SettingsManager.SaveSettings();
    }

    public void OnClickNextLevel()
    {
        Hide();
        if(LevelController.Instance !=null)
        {
            LevelController.Instance.LevelFinished();
        }
    }

    protected override void OnShow()
    {
        if (_levelTextMesh != null)
        {
            string levelText = LocalizationManager.Instance.GetLocalizedText("Menu_Level");
            _levelTextMesh.text = levelText + " " + LevelController.Instance.GetThisLevelId().ToString();
        }
    }
}
