using HSVPicker;
using UnityEngine;
using UnityEngine.UI;

public class CustomColorCustomizationController : MonoBehaviour
{
    [SerializeField]
    private Image _playerColorImage = null;

    [SerializeField]
    private Image _obstacleColorImage = null;

    [SerializeField]
    private Image _levelObjectsColorImage = null;

    [SerializeField]
    private Image _backgroundColorImage = null;

    [SerializeField]
    private ColorsPreset _customColorPreset = null;

    [SerializeField]
    private ColorPicker _colorPicker = null;

    // Start is called before the first frame update
    void OnEnable()
    {
        SetColorImages();
        ColorsManager.Instance.SetColorsPreset(_customColorPreset);

#if !UNITY_EDITOR
        AnalyticsManager.Instance.ReportCustomizationScreenVisit();
#endif
    }

    public void OnDisable()
    {
        _colorPicker.gameObject.SetActive(false);
    }

    public void SetColorImages()
    {
        _playerColorImage.color = new Color(_customColorPreset.PlayerColor.r, _customColorPreset.PlayerColor.g, _customColorPreset.PlayerColor.b, 1);
        _obstacleColorImage.color = new Color(_customColorPreset.ObstaclesColor.r, _customColorPreset.ObstaclesColor.g, _customColorPreset.ObstaclesColor.b, 1);
        _backgroundColorImage.color = new Color(_customColorPreset.BackgroundColor.r, _customColorPreset.BackgroundColor.g, _customColorPreset.BackgroundColor.b, 1);
        _levelObjectsColorImage.color = new Color(_customColorPreset.BaseLevelObjectsColor.r, _customColorPreset.BaseLevelObjectsColor.g, _customColorPreset.BaseLevelObjectsColor.b, 1);
    }
    public void OnClickedPlayerColor()
    {
        _colorPicker.onValueChanged.RemoveAllListeners();
        _colorPicker.gameObject.SetActive(true);
        _colorPicker.R = _customColorPreset.PlayerColor.r;
        _colorPicker.G = _customColorPreset.PlayerColor.g;
        _colorPicker.B = _customColorPreset.PlayerColor.b;    
        _colorPicker.onValueChanged.AddListener(OnPlayerColorPicked);
    }

    public void OnClickedObstaclesColor()
    {
        _colorPicker.onValueChanged.RemoveAllListeners();
        _colorPicker.gameObject.SetActive(true);
        _colorPicker.R = _customColorPreset.ObstaclesColor.r;
        _colorPicker.G = _customColorPreset.ObstaclesColor.g;
        _colorPicker.B = _customColorPreset.ObstaclesColor.b;
        _colorPicker.onValueChanged.AddListener(OnObstaclesColorPicked);
    }

    public void OnClickedBackgroundColor()
    {
        _colorPicker.onValueChanged.RemoveAllListeners();
        _colorPicker.gameObject.SetActive(true);
        _colorPicker.R = _customColorPreset.BackgroundColor.r;
        _colorPicker.G = _customColorPreset.BackgroundColor.g;
        _colorPicker.B = _customColorPreset.BackgroundColor.b;       
        _colorPicker.onValueChanged.AddListener(OnBackgroundColorPicked);
    }

    public void OnClickedLevelObjectsColor()
    {
        _colorPicker.onValueChanged.RemoveAllListeners();
        _colorPicker.gameObject.SetActive(true);
        _colorPicker.R = _customColorPreset.BaseLevelObjectsColor.r;
        _colorPicker.G = _customColorPreset.BaseLevelObjectsColor.g;
        _colorPicker.B = _customColorPreset.BaseLevelObjectsColor.b;   
        _colorPicker.onValueChanged.AddListener(OnLevelObjectsColorPicked);
    }

    public void OnPlayerColorPicked(Color color)
    {
        _customColorPreset.PlayerColor.r = _colorPicker.R;
        _customColorPreset.PlayerColor.g = _colorPicker.G;
        _customColorPreset.PlayerColor.b = _colorPicker.B;
        ColorsManager.Instance.SetColors();
        SetColorImages();
    }

    public void OnObstaclesColorPicked(Color color)
    {
        _customColorPreset.ObstaclesColor.r = _colorPicker.R;
        _customColorPreset.ObstaclesColor.g = _colorPicker.G;
        _customColorPreset.ObstaclesColor.b = _colorPicker.B;
        ColorsManager.Instance.SetColors();
        SetColorImages();
    }

    public void OnBackgroundColorPicked(Color color)
    {
        _customColorPreset.BackgroundColor.r = _colorPicker.R;
        _customColorPreset.BackgroundColor.g = _colorPicker.G;
        _customColorPreset.BackgroundColor.b = _colorPicker.B;
        ColorsManager.Instance.SetColors();
        SetColorImages();
    }

    public void OnLevelObjectsColorPicked(Color color)
    {
        _customColorPreset.BaseLevelObjectsColor.r = _colorPicker.R;
        _customColorPreset.BaseLevelObjectsColor.g = _colorPicker.G;
        _customColorPreset.BaseLevelObjectsColor.b = _colorPicker.B;
        ColorsManager.Instance.SetColors();
        SetColorImages();
    }
}
