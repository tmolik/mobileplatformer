using UnityEngine;

public class UiColorPresetButton : MonoBehaviour
{
    [SerializeField]
    private ColorsPreset _colorsPreset = null;

    public void OnClickPreset()
    {
        if (_colorsPreset == null)
            return;

        if (ColorsManager.Instance == null)
            return;

        ColorsManager.Instance.SetColorsPreset(_colorsPreset);
    }
}
