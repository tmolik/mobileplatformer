﻿using UnityEngine;

public class UILevelSelectMenu : UiBaseMenuController
{
    [SerializeField]
    private GlobalIntVariable _levelCountVariable = null;

    [SerializeField]
    private GameObject _levelButtonPrefab = null;

    [SerializeField]
    private RectTransform _levelsRect = null;

    // Start is called before the first frame update
    void Start()
    {
        CreateLevelsButtons();
    }

    private void CreateLevelsButtons()
    {
        for (int i = 1; i <= _levelCountVariable.Value; i++)
        {
            CreateLevelButton(i);
        }
    }

    private void CreateLevelButton(int levelID)
    {
        GameObject levelButtonGameObject = Instantiate(_levelButtonPrefab);
        levelButtonGameObject.transform.SetParent(_levelsRect);
        UiLevelButton levelButton = levelButtonGameObject.GetComponent<UiLevelButton>();
        levelButton.SetLevel(levelID);
    }

}
