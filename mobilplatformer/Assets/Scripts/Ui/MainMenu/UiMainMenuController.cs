﻿using UnityEngine;
using UnityEngine.UI;
using Molioo.UBB.Levels;

public class UiMainMenuController : MonoBehaviour
{
    [SerializeField]
    private UILevelSelectMenu _levelSelectMenu = null;

    [SerializeField]
    private RectTransform _colorsCustomizationMenuRect = null;

    [SerializeField]
    private RectTransform _menuButtonsRect = null;

    [SerializeField]
    private Button _continueButton = null;

    [SerializeField]
    private GlobalIntVariable _levelsCountVariable = null;

    private void Start()
    {
        SetContinueButtonInteractable();
        AdsManager.Instance.ShowBanner();
        CheckForAppReview();
    }

    public void OnClickLevelSelect()
    {
        _levelSelectMenu.Show();
        _menuButtonsRect.gameObject.SetActive(false);
    }

    public void OnClickCustomization()
    {
        _colorsCustomizationMenuRect.gameObject.SetActive(true);
        _menuButtonsRect.gameObject.SetActive(false);
    }

    public void OnClickContinue()
    {
        if (GameFilesManager.CurrentGameData != null)
        {
            LevelsManager.Instance.SelectedLevel(GameFilesManager.CurrentGameData.CurrentLevelID + 1);
        }
        else
        {
            LevelsManager.Instance.SelectedLevel(1);
        }
    }

    public void OnClickEndless()
    {
        LevelsManager.Instance.LoadEndlessLevel();
        AdsManager.Instance.HideBanner();
    }

    public void OnClickMute()
    {
        SettingsManager.CurrentSettings.IsMuted = !SettingsManager.CurrentSettings.IsMuted;
        SettingsManager.SaveSettings();
    }

    public void OnClickBackToMenu()
    {
        _levelSelectMenu.Hide();
        _colorsCustomizationMenuRect.gameObject.SetActive(false);
        _menuButtonsRect.gameObject.SetActive(true);
    }

    public void SaveColorsSettings()
    {
        if (ColorsManager.Instance != null)
            ColorsManager.Instance.SaveCurrentColorPreset();
    }

    public void OnClickShowAchievements()
    {
        if(SocialPlatformManager.Instance!=null)
        {
            SocialPlatformManager.Instance.ShowAchievements();
        }
    }

    public void OnClickShowLeaderboard()
    {
        if (SocialPlatformManager.Instance != null)
        {
            SocialPlatformManager.Instance.ShowLeaderboard();
        }
    }

    public void OnClickQuit()
    {
        Application.Quit();
    }

    private void SetContinueButtonInteractable()
    {
        if (GameFilesManager.CurrentGameData != null && _levelsCountVariable != null)
        {
            _continueButton.interactable = GameFilesManager.CurrentGameData.CurrentLevelID < _levelsCountVariable.Value;
        }
    }

    private void CheckForAppReview()
    {
        if (InAppReviewManager.Instance != null)
        {
            if (InAppReviewManager.Instance.CanShowInAppReview())
            {
                InAppReviewManager.Instance.RequestInAppReview();
            }
        }
    }
}
