﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Molioo.UBB.Levels;

public class UiLevelButton : MonoBehaviour
{
    private int _levelID = 0;

    [SerializeField]
    private TextMeshProUGUI _levelName = null;

    [SerializeField]
    private Button _button = null;


    public void OnClick()
    {
        LevelsManager.Instance.SelectedLevel(_levelID);
        AdsManager.Instance.HideBanner();
    }

    public void SetLevel(int levelID)
    {
        _levelID = levelID;
        _levelName.text = levelID.ToString();
        if (GameFilesManager.CurrentGameData != null)
        {
            _button.interactable = GameFilesManager.CurrentGameData.CurrentLevelID + 1 >= _levelID;
        }
    }
}
