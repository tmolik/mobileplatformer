﻿using Molioo.Localization;
using Molioo.UBB.Levels;
using TMPro;
using UnityEngine;

public class UiLevelWonMenuController : UiBaseMenuController
{
    [SerializeField]
    private TextMeshProUGUI _levelTextMesh;

    public void OnClickBackToMenu()
    {
        LevelsManager.Instance.LoadMenu();
        Hide();
    }

    public void OnClickNextLevel()
    {
        LevelsManager.Instance.LoadNextLevel();
        Hide();
    }

    protected override void OnShow()
    {
        if (_levelTextMesh != null)
        {
            string levelText = LocalizationManager.Instance.GetLocalizedText("Menu_Level");
            _levelTextMesh.text = levelText + " " + LevelController.Instance.GetThisLevelId().ToString();
        }
    }
}
