﻿using TMPro;
using UnityEngine;

public class UiEndlessTimeTextController : Singleton<UiEndlessTimeTextController>
{
    [SerializeField]
    private TextMeshProUGUI _timeText = null;

    [SerializeField]
    private GlobalFloatVariable _endlessTimeVariable = null;

    public void Update()
    {
        UpdateTimeText();
    }

    private void UpdateTimeText()
    {
        if (_timeText == null || _endlessTimeVariable == null)
            return;

        if (GameManager.CurrentGameMode != EGameMode.Endless)
            _timeText.text = "";
        else
            _timeText.text = _endlessTimeVariable.Value == 0 ? "" : _endlessTimeVariable.Value.ToString("0");
    }

    public void Clear()
    {
        _timeText.text = "";
    }
}
