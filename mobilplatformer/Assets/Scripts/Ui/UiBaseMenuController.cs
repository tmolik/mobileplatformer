﻿using UnityEngine;

public class UiBaseMenuController : MonoBehaviour
{
    [SerializeField]
    private RectTransform _menuRect = null;

    [SerializeField]
    private bool _pauseTimeOnEnable = false;

    private bool _isShown = false;

    public void Show()
    {
        _isShown = true;
        _menuRect.gameObject.SetActive(true);
        if (_pauseTimeOnEnable)
            Time.timeScale = 0;

        OnShow();
    }

    public void Hide()
    {
        _isShown = false;
        _menuRect.gameObject.SetActive(false);
        if (_pauseTimeOnEnable)
            Time.timeScale = 1;

        OnHide();
    }

    protected virtual void OnShow()
    {

    }

    protected virtual void OnHide()
    {

    }


    public void Switch()
    {
        if (_isShown)
            Hide();
        else
            Show();
    }
}
