﻿using UnityEngine;
using UnityEngine.UI;
using Molioo.UBB.Levels;

public class UiEndlessModeSummaryController : UiBaseMenuController
{
    [SerializeField]
    private GlobalFloatVariable _endlessTimeVariable = null;

    [SerializeField]
    private Button _watchAdButton = null;

    public void OnClickBackToMenu()
    {
        if (GameManager.CurrentGameMode == EGameMode.Endless && EndlessModeController.Instance != null)
        {
            EndlessModeController.Instance.ClearLevelParts();
        }
        LevelsManager.Instance.LoadMenu();
        _endlessTimeVariable.Value = 0;
        Hide();
    }

    public void OnClickTryAgain()
    {
        if (GameManager.CurrentGameMode == EGameMode.Endless && EndlessModeController.Instance != null)
        {
            EndlessModeController.Instance.ClearLevelParts();
        }
        LevelsManager.Instance.ResetLevel();
        _endlessTimeVariable.Value = 0;
        Hide();
    }

    public void OnClickWatchAdToContinue()
    {
        AdsManager.Instance.ShowRewardedVideo(WatchedAd);
        EndlessLevelController.ContinueWithWatchPossible = false;
        _watchAdButton.interactable = false;
    }

    public void WatchedAd()
    {
        (EndlessLevelController.Instance as EndlessLevelController).RestartEndless();
        Hide();
    }

    protected override void OnShow()
    {
        if(_watchAdButton!=null)
        {
            _watchAdButton.interactable = AdsManager.Instance.IsRewardedVideoReady() && EndlessLevelController.ContinueWithWatchPossible;
        }
    }

    public void Update()
    {
        HandleDebugInput();   
    }

    private void HandleDebugInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            (EndlessLevelController.Instance as EndlessLevelController).RestartEndless();
            Hide();
        }
    }
}
