﻿public class UiGameplayManager : UiBaseMenuController
{
    public void OnClickPause()
    {
        UiManager.Instance.PauseMenu.Show();
    }

    public void OnClickRestart()
    {
        if (GameManager.CurrentGameMode == EGameMode.Endless && EndlessModeController.Instance != null)
        {
            EndlessModeController.Instance.ClearLevelParts();
        }
        Molioo.UBB.Levels.LevelsManager.Instance.ResetLevel();
    }


}
