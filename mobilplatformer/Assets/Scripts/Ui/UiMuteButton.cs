using UnityEngine;
using UnityEngine.UI;

public class UiMuteButton : MonoBehaviour
{
    [SerializeField]
    private Image _muteIconImage;

    [SerializeField]
    private Sprite _muteOffIconSprite;

    [SerializeField]
    private Sprite _muteOnIconSprite;

    private void OnEnable()
    {
        UpdateButtonVisual();
    }


    public void UpdateButtonVisual()
    {
        _muteIconImage.sprite = SettingsManager.CurrentSettings.IsMuted ? _muteOnIconSprite : _muteOffIconSprite;
    }
}
