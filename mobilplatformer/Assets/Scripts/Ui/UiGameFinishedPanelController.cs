using UnityEngine;

public class UiGameFinishedPanelController : UiBaseMenuController
{
    public void OnClickBackToMenu()
    {
        Molioo.UBB.Levels.LevelsManager.Instance.LoadMenu();
        Hide();
    }

    public void OnClickSendFeedback()
    {
        Application.OpenURL("mailto://molioogames@gmail.com");
    }
}
