﻿using UnityEngine;

public class UiManager : Singleton<UiManager>
{
    public UiLevelWonMenuController LevelWonMenu;

    public UiGameFinishedPanelController GameFinishedMenu;

    public UiEndlessModeSummaryController EndlessSummaryMenu;

    public UiPauseMenuController PauseMenu;

    public UiGameplayManager GameplayUI;

    private void Update()
    {
        HandlePauseInput();
    }

    private void HandlePauseInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && GameManager.CurrentGamePhase == EGamePhase.Game)
            PauseMenu.Switch();
    }
}
