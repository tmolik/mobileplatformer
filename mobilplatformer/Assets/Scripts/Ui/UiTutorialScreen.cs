﻿using UnityEngine;
using Molioo.UBB.Tutorial;

public class UiTutorialScreen : MonoBehaviour
{
    [SerializeField]
    private GameObject _tapToCloseTextGameObject = null;

    [SerializeField]
    private float _timeToEnableClosing = 1f;

    private float _aliveTimer = 0;

    public void OnEnable()
    {
        _aliveTimer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        CheckTimeToEnableClosing();
    }

    private void CheckTimeToEnableClosing()
    {
        _aliveTimer += Time.unscaledDeltaTime;
        if (_aliveTimer > _timeToEnableClosing)
            _tapToCloseTextGameObject.gameObject.SetActive(true);
    }

    public void OnClick()
    {
        if (_aliveTimer > _timeToEnableClosing)
        {
            gameObject.SetActive(false);
            TutorialManager.Instance.TutorialClosed();
        }
    }
}
