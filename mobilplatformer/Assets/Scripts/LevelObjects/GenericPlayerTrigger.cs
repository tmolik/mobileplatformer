﻿using UnityEngine;
using UnityEngine.Events;

public class GenericPlayerTrigger : MonoBehaviour
{
    [SerializeField]
    private UnityEvent _onPlayerTrigger = new UnityEvent();

    [SerializeField]
    private bool _invokeOnlyOnce = false;

    private bool _alreadyInvoked = false;

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (_invokeOnlyOnce && _alreadyInvoked)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            if (_onPlayerTrigger != null)
                _onPlayerTrigger.Invoke();
            _alreadyInvoked = true;
        }
    }
}
