﻿using UnityEngine;
using UnityEngine.Events;

public class GameButton : MonoBehaviour
{
    private const string BUTTON_PRESSED_ANIM_NAME = "ButtonPressed";
    private const string BUTTON_NOT_PRESSED_ANIM_NAME = "ButtonNotPressed";

    [SerializeField]
    private UnityEvent _onPressButton = new UnityEvent();

    private bool _pressed = false;

    private Animator _buttonAnimator = null;

    public void Awake()
    {
        _buttonAnimator = GetComponent<Animator>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        PlayerCubeController playerController = other.attachedRigidbody.GetComponent<PlayerCubeController>();
        if (playerController != null)
        {
            PlayerPressedButton();
        }
    }
    
    private void PlayerPressedButton()
    {
        if (_pressed)
            return;

        _pressed = true;
        if (_onPressButton != null)
            _onPressButton.Invoke();

        _buttonAnimator.CrossFade(BUTTON_PRESSED_ANIM_NAME, 1f);
    }

    public void ResetButton()
    {
        _buttonAnimator.CrossFade(BUTTON_NOT_PRESSED_ANIM_NAME, 0.1f);
        _pressed = false;
    }
}
