﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private float _speed = 2f;

    public void Update()
    {
        transform.position += transform.forward * _speed * Time.deltaTime;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null)
        {
            if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
            {
                Molioo.UBB.Levels.LevelController.Instance.PlayerDied(playerController);
            }
        }

        gameObject.SetActive(false);
    }

    public void OnCollisionEnter(Collision collision)
    {
        gameObject.SetActive(false);
    }
}
