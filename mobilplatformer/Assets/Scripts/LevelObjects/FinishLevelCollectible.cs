﻿using UnityEngine;
using Molioo.UBB.Levels;

public class FinishLevelCollectible : MonoBehaviour
{
    [SerializeField]
    private float _amplitude = 1;

    [SerializeField]
    private float _speed = 2;

    private float _yPosition = 0;
    private float _newYPositon = 0;

    private void Start()
    {
        _yPosition = transform.position.y;
    }

    public void Update()
    {
        _newYPositon = _yPosition + _amplitude * Mathf.Sin(_speed * Time.time);
        transform.position = new Vector3(transform.position.x, _newYPositon, transform.position.z);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            LevelController.Instance.LevelFinished();
        }
    }

    public void OnValidate()
    {
#if UNITY_EDITOR
        UnityEditor.SceneVisibilityManager.instance.DisablePicking(transform.GetChild(0).gameObject, false);
#endif
    }
}
