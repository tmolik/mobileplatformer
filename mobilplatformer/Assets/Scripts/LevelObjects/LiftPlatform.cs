﻿using UnityEngine;

public class LiftPlatform : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigidbody = null;

    [SerializeField]
    private float _moveSpeed = 2f;

    private bool _isPlayerOnPlatform = false;


    private void FixedUpdate()
    {
        _rigidbody.velocity = _isPlayerOnPlatform ? Vector3.up * _moveSpeed : Vector3.zero;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            _isPlayerOnPlatform = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            _isPlayerOnPlatform = false;
        }
    }
}
