﻿using UnityEngine;

public class PlayerKillerTrigger : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if(other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            Molioo.UBB.Levels.LevelController.Instance.PlayerDied(playerController);
        }
    }
}
