﻿using UnityEngine;

public class RigidbodyObstacle : PlayerKillerTrigger
{
    [SerializeField]
    private bool _activateOnEnable = false;

    private Rigidbody _rigidbody = null;

    private void OnEnable()
    {
        if (_activateOnEnable)
            EnableObstacle();
    }

    public void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void EnableObstacle()
    {
        if(_rigidbody==null)
            _rigidbody = GetComponent<Rigidbody>();

        _rigidbody.useGravity = true;
        _rigidbody.isKinematic = false;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
            EnableObstacle();
    }

    private void OnDisable()
    {
        if (_rigidbody == null)
            _rigidbody = GetComponent<Rigidbody>();

        _rigidbody.useGravity = false;
        _rigidbody.isKinematic = true;
        _rigidbody.velocity = Vector3.zero;
    }
}
