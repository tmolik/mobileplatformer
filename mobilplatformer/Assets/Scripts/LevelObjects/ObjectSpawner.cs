﻿using Molioo.ObjectPooling;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField]
    private EPoolables _poolableToSpawn = EPoolables.Projectile;

    [SerializeField]
    private Vector2 _spawnIntervalRange = new Vector2(2, 2);

    [SerializeField]
    private Transform _spawnTransform;

    [SerializeField]
    private bool _spawnAtAwake = false;

    private float _spawnTimer = 0;
    private float _currentTimeToSpawn = 0;

    // Start is called before the first frame update
    void Start()
    {
        _currentTimeToSpawn = _spawnIntervalRange.GetRandomValueInRange();
        _spawnTimer = _spawnAtAwake? _currentTimeToSpawn :  0;
    }

    // Update is called once per frame
    void Update()
    {
        _spawnTimer += Time.deltaTime;
        if (_spawnTimer >= _currentTimeToSpawn)
        {
            SpawnObject();
            _spawnTimer = 0;
            _currentTimeToSpawn = _spawnIntervalRange.GetRandomValueInRange();
        }
    }

    private void SpawnObject()
    {
        GameObject spawnedGameObject = ObjectPooler.Get.GetPooledObject(_poolableToSpawn);
        if (spawnedGameObject != null)
        {
            spawnedGameObject.transform.position = _spawnTransform.position;
            spawnedGameObject.transform.rotation = _spawnTransform.rotation;
            spawnedGameObject.transform.SetParent(transform);
            spawnedGameObject.SetActive(true);
        }

    }

    public void SetNewSpawnTime(float time)
    {
        _spawnIntervalRange = new Vector2(time,time);
    }
}
