﻿using UnityEngine;

public class MovingWallOfDeath : MonoBehaviour
{
    [SerializeField]
    private Vector3 _direction = Vector3.right;

    [SerializeField]
    private float _speed = 3f;

    private bool _isMoving = false;

    public void FixedUpdate()
    {
        if (!_isMoving)
            return;
        transform.position += _direction.normalized * _speed * Time.fixedDeltaTime;
    }

    public void StartMoving()
    {
        _isMoving = true;
    }

    public void InverseDirection()
    {
        _direction *= -1;
    }
}
