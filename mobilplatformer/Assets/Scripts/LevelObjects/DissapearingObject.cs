﻿using System.Collections;
using UnityEngine;

public class DissapearingObject : MonoBehaviour
{
    [SerializeField]
    private EDisappearType _disappearType = EDisappearType.OnPlayerTriggerExit;

    [SerializeField]
    private float _disappearTime = 1f;

    private bool _isDisappearing = false;

    [Header("Dissappear animation")]
    [SerializeField]
    private bool _useScaleDownAnimation = false;
    [SerializeField]
    private float _scaleDownAnimationTime = 0.5f;

    private float _animationTimer = 0;
    private bool _isScalingDown = false;
    private Vector3 _localScaleAtBeginningOfAnimation;

    public void Awake()
    {
        _localScaleAtBeginningOfAnimation = transform.localScale;
    }

    public void Update()
    {
        if (_isScalingDown)
        {
            _animationTimer += Time.deltaTime;
            float easeValue = FEasing.EaseOutCubic(0f, 1f, _animationTimer / _scaleDownAnimationTime);
            transform.localScale = Vector3.Lerp(_localScaleAtBeginningOfAnimation, Vector3.zero, easeValue);
            if (_animationTimer >= _scaleDownAnimationTime)
            {
                _isScalingDown = false;
                gameObject.SetActive(false);
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (_disappearType == EDisappearType.OnPlayerTriggerExit)
            return;

        if (other.attachedRigidbody == null)
            return;

        if(other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            Disappear();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_disappearType == EDisappearType.OnPlayerTriggerEnter)
            return;

        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            Disappear();
        }
    }

    private void Disappear()
    {
        if (_isDisappearing)
            return;

        StartCoroutine(DisappearRoutine());
    }

    private IEnumerator DisappearRoutine()
    {
        _isDisappearing = true;
        yield return new WaitForSeconds(_disappearTime);
        if (_useScaleDownAnimation)
        {
            _isScalingDown = true;
            _animationTimer = 0;
            _localScaleAtBeginningOfAnimation = transform.localScale;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void ShowAgain()
    {
        gameObject.SetActive(true);
        _isScalingDown = false;
        transform.localScale = _localScaleAtBeginningOfAnimation;
        _isDisappearing = false;
    }

    private enum EDisappearType
    {
        OnPlayerTriggerEnter,
        OnPlayerTriggerExit
    }
}
