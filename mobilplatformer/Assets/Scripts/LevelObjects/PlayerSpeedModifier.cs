using UnityEngine;

public class PlayerSpeedModifier : MonoBehaviour
{
    [SerializeField]
    private float _speedMultiplierToApply = 0.5f;

    private float _previousSpeedMultiplier = 1;

    private bool _alreadyInvoked = false;

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (_alreadyInvoked)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController player))
        {
            _previousSpeedMultiplier = player.SpeedMultiplier;
            player.SpeedMultiplier = _speedMultiplierToApply;
            _alreadyInvoked = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController player))
        {
            player.SpeedMultiplier = _previousSpeedMultiplier;
        }
    }

    public void Reset()
    {
        _alreadyInvoked = false;
    }
}
