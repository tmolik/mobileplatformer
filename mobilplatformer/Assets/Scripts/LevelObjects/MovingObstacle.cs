﻿using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : PlayerKillerTrigger
{
    [SerializeField]
    private List<Transform> _targetPoints = new List<Transform>();

    [SerializeField]
    private float _movementSpeed = 2f;

    [SerializeField]
    private bool _usePingPongDestinations = false;

    [SerializeField]
    private bool _teleportToFirstTargetPointAtStart = false;

    [SerializeField]
    private bool _checkOnlyXposition = false;

    private Vector3 _direction = Vector3.zero;

    private int _currentTargetIndex = -1;
    private int _currentDestinationIndexesChange = 1;
    private Vector3 _currentDestinationPosition;

    public void Start()
    {
        ChangeDestination();
        if (_teleportToFirstTargetPointAtStart)
            transform.position = _currentDestinationPosition;
    }

    public void Update()
    {
        if (_currentTargetIndex != -1)
        {
            transform.position += _direction * _movementSpeed * Time.deltaTime;

            if (IsAtPosition())
            {
                ChangeDestination();
            }
        }
    }

    private void ChangeDestination()
    {
        _currentTargetIndex += _currentDestinationIndexesChange;

        if (!_usePingPongDestinations)
        {
            if (_currentTargetIndex >= _targetPoints.Count)
                _currentTargetIndex = 0;
        }
        else
        {
            if (_currentTargetIndex >= _targetPoints.Count && _currentDestinationIndexesChange == 1)
            {
                _currentTargetIndex = _targetPoints.Count - 2;
                _currentDestinationIndexesChange = -1;
            }
            else if (_currentTargetIndex <= -1 && _currentDestinationIndexesChange == -1)
            {
                _currentTargetIndex = 1;
                _currentDestinationIndexesChange = 1;
            }
        }

        SetNewDestination();
    }

    private void SetNewDestination()
    {
        _currentDestinationPosition = _targetPoints[_currentTargetIndex].position;
        _direction = (_currentDestinationPosition - transform.position).normalized;
    }

    private bool IsAtPosition()
    {
        if (_checkOnlyXposition)
        {
            return Mathf.Abs(transform.position.x - _currentDestinationPosition.x) < 0.2f;
        }
        else
        {
            return Vector3.Distance(transform.position, _currentDestinationPosition) < 0.2f;
        }
    }
}
