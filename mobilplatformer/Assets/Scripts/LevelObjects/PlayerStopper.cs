﻿using UnityEngine;

public class PlayerStopper : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            playerController.StopMoving();
            Destroy(gameObject);
        }
    }
}
