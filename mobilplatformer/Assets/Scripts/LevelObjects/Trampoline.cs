﻿using UnityEngine;

public class Trampoline : MonoBehaviour
{
    [SerializeField]
    private float _bouncePower = 10f;

    private Animator _animator = null;

    private const string TRAMPOLINE_USED_ANIMATION_NAME = "TrampolineUsed";

    private void Awake()
    {
        _animator = GetComponent<Animator>();    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            playerController.Bounce(_bouncePower);
            _animator.CrossFade(TRAMPOLINE_USED_ANIMATION_NAME,0.05f);
        }
    }
}
