﻿using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField]
    private Transform _destinationTransform = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
            return;

        if (other.attachedRigidbody.TryGetComponent(out PlayerCubeController playerController))
        {
            GoThroughPortal(playerController);
        }
    }

    private void GoThroughPortal(PlayerCubeController player)
    {
        player.transform.position = _destinationTransform.position;
        player.TrailRenderer.Clear();
        CameraController.Instance.InstantyGoToTargetPosition();
    }
}
