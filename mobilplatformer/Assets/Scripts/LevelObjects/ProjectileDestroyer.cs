using UnityEngine;

public class ProjectileDestroyer : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<Projectile>(out Projectile proj))
        {
            proj.gameObject.SetActive(false);
        }
    }
}
