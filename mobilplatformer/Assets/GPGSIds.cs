// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class GPGSIds
{
        public const string leaderboard_endless_game_mode_high_score = "CgkI-sa66KcaEAIQBw"; // <GPGSID>
        public const string achievement_survive_5_minutes = "CgkI-sa66KcaEAIQBg"; // <GPGSID>
        public const string achievement_beat_10_levels = "CgkI-sa66KcaEAIQAQ"; // <GPGSID>
        public const string achievement_beat_25_levels = "CgkI-sa66KcaEAIQAg"; // <GPGSID>
        public const string achievement_beat_50_levels = "CgkI-sa66KcaEAIQAw"; // <GPGSID>
        public const string achievement_beat_75_levels = "CgkI-sa66KcaEAIQBA"; // <GPGSID>
        public const string achievement_survive_1_minute = "CgkI-sa66KcaEAIQBQ"; // <GPGSID>

}

